/**************************************************************************//**
 * @item     CosyOS-II Config
 * @file     mcucfg_cmx.h
 * @brief    CMSIS Cortex-M Core Config File
 * @author   迟凯峰
 * @version  V2.1.1
 * @date     2024.04.17
 ******************************************************************************/

#ifndef __MCUCFG_CMX_H
#define __MCUCFG_CMX_H

/******************************************************************************
 *                             USER Definitions                               *
 ******************************************************************************/

          //*** <<< Use Configuration Wizard in Context Menu >>> ***//

///////////////////////////////////////////////////////////////////////////////
// <h> 任务栈配置
// <i> 任务栈配置

// <o> 系统启动任务的任务栈大小（Bytes）
// <i> 最小值：
// <i> 未启用浮点寄存器：56/64B；启用浮点寄存器：192/200B。
#define MCUCFG_STACKSIZE_STARTER        1024

// <o> 系统空闲任务的任务栈大小（Bytes）
// <i> 最小值：
// <i> 未启用浮点寄存器：56/64B；启用浮点寄存器：192/200B。
#define MCUCFG_STACKSIZE_SYSIDLE        512

// </h>
///////////////////////////////////////////////////////////////////////////////
// <o> 系统方案配置
// <0=> 全局寄存器变量 <1=> 互斥访问指令 <2=> 关中断
// <i> 方案一、全局寄存器变量
// <i> 该方案适用于所有ARM内核并具有最高的性能，可实现全局不关总中断、零中断延迟。但要求用户必须做到以下三点注意事项：
// <i> (1) 编译器必须开启对GNU/GCC的支持。
// <i> (2) 项目中所有C文件（CosyOS系统文件除外）均需直接或间接包含os_link.h，以声明全局寄存器变量r10、r11。
// <i> (3) 部分标准库/微库函数不能被调用（如printf），原因是它们已经被编译使用了r10、r11。
// <i> 方案二、互斥访问指令
// <i> 该方案仅适用于Cortex-M3/M4/M7等支持[LDREX/STREX]指令的内核，可实现全局不关总中断、零中断延迟。
// <i> 该方案不需要方案一中叙述的三点注意事项。
// <i> 方案三、关中断
// <i> 该方案适用于所有ARM内核，并具有极短的、确定的关闭总中断时间（包括开中断在内，不超过10个指令周期）。
// <i> 该方案，内核关闭总中断仅发生在中断挂起服务装载器中的__LOAD段。
// <i> 该方案不需要方案一中叙述的三点注意事项。
// <i> 总结：
// <i> 简单来说，方案一适用于高手，方案二适用于特定的内核，方案三适用于小白。
#define MCUCFG_SYSSCHEME                1
///////////////////////////////////////////////////////////////////////////////
// <o> 系统中断配置
// <0=> SysTick_Handler + PendSV_Handler <1=> TIMn_IRQHandler + XXX_IRQHandler
// <i> 首先，两种配置方案在性能上旗鼓相当，难分伯仲。
// <i> 1、SysTick_Handler + PendSV_Handler：
// <i> 要求MCU必须有BASEPRI寄存器，同时您不能调用xMaskingPRI和xResumePRI进出全局临界区，也不允许私自使用BASEPRI寄存器。
// <i> 如Cortex-M0/M0+/M23等内核，都没有BASEPRI寄存器，就不能采用此方案。
// <i> 又如虽使用了Cortex-M3/M4/M33/M7等内核，但确想调用xMaskingPRI和xResumePRI来实现不同掩蔽范围的全局临界区保护，也不能采用此方案。
// <i> 2、TIMn_IRQHandler + XXX_IRQHandler：
// <i> 用TIMn替代SysTick，TIMn_IRQHandler替代SysTick_Handler，XXX_IRQHandler替代PendSV_Handler。
// <i> CosyOS将不会使用PendSV_Handler和BASEPRI寄存器，如果MCU有BASEPRI寄存器，您可调用xMaskingPRI和xResumePRI进出全局临界区。
// <i> 但仍会使用SysTick定时器，用于任务管理器的时间统计。
// <i> 采用此方案，需在文本编辑界面中配置下方的用户定义项。
// <i> TIMn_IRQHandler 与 XXX_IRQHandler 必须满足如下关系：TIMn_IRQn / 32 == XXX_IRQn / 32。
// <i> 下方的用户定义项已经给出示例：TIM2_IRQHandler + EXTI0_IRQHandler，对于Cortex-M3/M4，直接可用。
#define MCUCFG_SYSINT                   0

/* 用户定义 */                          #if MCUCFG_SYSINT

// 1、PendSV_Handler替代中断-名称
#define PendSV2_Handler                 EXTI0_IRQHandler

// 2、PendSV_Handler替代中断-向量号
#define MCUCFG_PENDSVIRQ                EXTI0_IRQn

// 3、SysTick_Handler替代中断-名称
#define SysTick2_Handler                TIM2_IRQHandler

// 4、SysTick_Handler替代中断-向量号
#define MCUCFG_SYSTICKIRQ               TIM2_IRQn

// 5、SysTick_Handler替代中断-中断清零（清中断标志位）
#define mSysTick_Clear                  TIM2->SR = 0

// 6、SysTick的替代定时器-配置
#define mSysTick2_INIT \
do{ \
	/* 定时器中断的优先级，系统根据中断向量号自动配置为最低级 */ \
	mSysTick2_Priority; \
	/* 定时器时钟使能 */ \
	RCC->APB1ENR |= (1 << 0); \
	/* 预分频，本示例，APB1CLK = HCLK / 4 */ \
    TIM2->PSC = (SYSCFG_SYSCLK / 1000000) / 4 * 2 - 1; \
	/* 重装载值 */ \
    TIM2->ARR = SYSCFG_SYSTICKCYCLE - 1; \
	/* 使能计数器 | 只有上溢/下溢会生成更新中断 | 计数方向任意 */ \
	TIM2->CR1 = (1 << 0) | (1 << 2); \
	/* 使能更新中断 */ \
	TIM2->DIER = 1; \
}while(false)

#if MCUCFG_PENDSVIRQ / 32 != MCUCFG_SYSTICKIRQ / 32
#error MCUCFG_PENDSVIRQ / 32 != MCUCFG_SYSTICKIRQ / 32
#endif

/* 系统定义 */                          #else
#define PendSV2_Handler                 PendSV_Handler
#define SysTick2_Handler                SysTick_Handler
#define mSysTick_Clear                  do{}while(false)
                                        #endif
///////////////////////////////////////////////////////////////////////////////
// <o> 系统滴答时钟源
// <0=> 外部时钟 <1=> 内核时钟
// <i> 在此配置系统滴答时钟源，您无需再额外配置。
// <i> 如果您使用了外部晶振且系统时钟为8的整数倍，可配置系统滴答时钟源为外部时钟，以提高系统滴答的精度。
#define MCUCFG_SYSTICKCLKSOURCE         1
///////////////////////////////////////////////////////////////////////////////
// <o> PendSV_FIFO深度
// <i> 此项参数取决于您在中断中调用挂起服务的总数及中断的频率。
// <i> 对于Cortex-M来说，CosyOS规定PendSV_FIFO的最大深度为255。
// <i> 可开启PendSV_FIFO监控功能，监控历史上的最大值，再适当增大，以确保其不会溢出。
#define MCUCFG_PENDSVFIFO_DEPTH         64
#if MCUCFG_PENDSVFIFO_DEPTH > 255
#error PendSV_FIFO深度值溢出！
#endif
///////////////////////////////////////////////////////////////////////////////

                //*** <<< end of configuration section >>> ***//

/******************************************************************************
 *                        Compiler Related Definitions                        *
 ******************************************************************************/

#include SYSCFG_STANDARDHEAD
#if __FPU_PRESENT == __ENABLED__ && __FPU_USED == __ENABLED__
#define MCUCFG_HARDWAREFPU  __ENABLED__
#else
#define MCUCFG_HARDWAREFPU  __DISABLED__
#endif

/******************************************************************************
 *                               OS Definitions                               *
 ******************************************************************************/

/* Header */
#include <string.h>
#include "..\System\os_base.h"

/* Memory */
#define _SYS_MEM_
#define _CODE_MEM_
#define _CONST_MEM_
#define _STACK_MEM_
#define _MALLOC_MEM_
#define _XDATA_MEM_
#define _OBJ_MEM_

/* Register */
#define _SYS_REG_

/* Typedef */
typedef unsigned long long int s_u64_t;
typedef s_bool_t m_bit_t;
typedef s_u32_t  m_sp_t;
typedef s_u32_t  m_taskmsg_t;
typedef s_u32_t  m_fetion_t;
typedef s_u32_t  m_stacksize_t;
typedef s_u32_t  m_pc_t;
typedef s_u32_t  m_tick_t;
typedef s_u32_t  m_group_t;
#define m_boolvoid_tf *(s_boolvoid_tfp)

/* Extern */
extern s_u32_t  m_basepri;
extern s_u32_t *m_taskmsg_psp;
extern s_u8_t   mPendSV_FIFO_DepthMAX;
extern   void  *mPendSV_FIFO[2][MCUCFG_PENDSVFIFO_DEPTH + 1];
#if MCUCFG_SYSSCHEME == 0
register void **mPendSV_P0 __ASM("r10");
register void **mPendSV_P1 __ASM("r11");
#else
extern void **mPendSV_P0;
extern void **mPendSV_P1;
#endif
extern void mPendSV_Loader(void *);
extern void mPendSV_Handler(void);
extern void mTaskStack_PUSHPOP(s_bool_t, void *);

/* CONST & ATTRIBUTE */
#define MCUCFG_ISA                __ARM__
#define MCUCFG_GNU                __ENABLED__
#define MCUCFG_NOP                __NOP()
#define MCUCFG_PCLEN              4
#define MCUCFG_USING
#define MCUCFG_C51USING
#define MCUCFG_SYSTICK_ATTRIBUTE
#define MCUCFG_PENDSV_ATTRIBUTE
#define MCUCFG_TASKMSG_TYPE       1
#define MCUCFG_TASKMSG_PSP        m_taskmsg_psp = (s_u32_t *)__m_get_psp()
#define MCUCFG_TASKMSG_VAR        m_taskmsg_t r0__, m_taskmsg_t r1__, m_taskmsg_t r2__, m_taskmsg_t r3__
#define MCUCFG_TASKMSG_VAL        0, 0, 0, 0
#define MCUCFG_TASKMSG_SIZE       ((&m0 - &m0_ - 1) * 4)
#define MCUCFG_TERNARYMASK
#if MCUCFG_HARDWAREFPU == __ENABLED__
#define MCUCFG_CALLER_PUSH_FPU    (18 * 4) /** \push   {s0-s15,FPSCR,UNKNOW} */
#define MCUCFG_CALLEE_PUSH_FPU    (16 * 4) /** \vstmdb {s16-s31} */
#else
#define MCUCFG_CALLER_PUSH_FPU    0
#define MCUCFG_CALLEE_PUSH_FPU    0
#endif
#define MCUCFG_CALLER_PUSH_REG    (8 * 4)  /** \push   {r0-r3,r12,r14(lr),r15(pc),xPSR} */
#if MCUCFG_SYSSCHEME == 0
#define MCUCFG_CALLEE_PUSH_REG    (6 * 4)  /** \stmdb  {r4-r9} */
#define MCUCFG_CALLEE_REG         r4-r9
#else
#define MCUCFG_CALLEE_PUSH_REG    (8 * 4)  /** \stmdb  {r4-r11} */
#define MCUCFG_CALLEE_REG         r4-r11
#endif
#define MCUCFG_CALLER_PUSH        (MCUCFG_CALLER_PUSH_FPU + MCUCFG_CALLER_PUSH_REG)
#define MCUCFG_CALLEE_PUSH        (MCUCFG_CALLEE_PUSH_FPU + MCUCFG_CALLEE_PUSH_REG)
#define MCUCFG_BASICSTACKSIZE     (MCUCFG_CALLER_PUSH + MCUCFG_CALLEE_PUSH)
#define MCUCFG_STACK_ALIGN        __align(8)
#define MCUCFG_TASKSTACK_REALLOC  __DISABLED__
#define MCUCFG_STACKSIZE_TASKMGR  (MCUCFG_BASICSTACKSIZE * 2 + (MCUCFG_HARDWAREFPU == __ENABLED__ ? 0 : 24))
#define MCUCFG_STACKSIZE_DEBUGGER (MCUCFG_BASICSTACKSIZE * 2)

/*
 * MCUAPI
 */

/* TaskNode */
#define mTaskNode_Head_           m_sp_t psp;
#define mTaskNode_Tail_           m_sp_t psp_top;

/* SysTick */
#define mSysTick_CLKMOD           (MCUCFG_SYSTICKCLKSOURCE ? 1 : 8)
#define mSysTick_Cycle            (SYSCFG_SYSCLK / (1000000UL / SYSCFG_SYSTICKCYCLE) / mSysTick_CLKMOD)
#if mSysTick_Cycle > 0x00FFFFFF
#error 系统滴答定时器溢出，必须减小系统时钟或系统滴答周期。
#elif 1000000UL % SYSCFG_SYSTICKCYCLE
#warning 每秒钟的系统滴答周期数不为整数，建议重新调整系统滴答周期。
#elif SYSCFG_SYSCLK % (1000000UL / SYSCFG_SYSTICKCYCLE)
#warning 每秒钟的系统滴答周期数不为整数，建议重新调整系统时钟或系统滴答周期。
#elif SYSCFG_SYSCLK / (1000000UL / SYSCFG_SYSTICKCYCLE) % mSysTick_CLKMOD
#warning 每秒钟的系统滴答周期数不为整数，建议重新调整系统时钟或系统滴答周期。
#endif
#define mSysTick_InitValue        mSysTick_Cycle
#define mSysTick_Counter          SysTick->VAL
#define mSysTick_CtrlReg          SysTick->CTRL = (MCUCFG_SYSTICKCLKSOURCE ? 0x04 : 0x00) | 0x01
#define mSysTick_Enable           SysTick->CTRL|= 0x02
#define mSysTick_Priority         *(volatile s_u8_t  *)0xE000ED23 = 0xFF
#define mSysTick_INIT \
do{ \
	SysTick->LOAD = mSysTick_InitValue; \
	mSysTick_Priority; \
	mSysTick_CtrlReg; \
}while(false)

/* 系统中断 */
#if !MCUCFG_SYSINT
/* 0、SysTick_Handler + PendSV_Handler */
#define mSysTick2_INIT            mSysTick_Enable
#define mPendSV_Priority          *(volatile s_u8_t  *)0xE000ED22 = 0xFF
#define mPendSV_Set               *(volatile s_u32_t *)0xE000ED04 = 0x10000000
#define mPendSV_Clear
#define mPendSV_INIT \
do{ \
	mPendSV_Priority; \
	m_basepri <<= 7 - ((*(volatile s_u32_t *)0xE000ED00 >> 8) & 7); \
	m_basepri--; \
	m_basepri <<= 1 + ((*(volatile s_u32_t *)0xE000ED00 >> 8) & 7); \
}while(false)
#define mSysIRQ_Disable           __mu_disable_sysirq()
#define mSysIRQ_Enable            __mx_resume_pri(0)
#else
/* 1、TIMn_IRQHandler + XXX_IRQHandler */
#define mSysTick2_Priority        *(volatile s_u32_t *)(0xE000E400 + MCUCFG_SYSTICKIRQ / 4 * 4)|= 0xFFUL << (MCUCFG_SYSTICKIRQ % 4) * 8
#define mPendSV_Priority          *(volatile s_u32_t *)(0xE000E400 + MCUCFG_PENDSVIRQ /  4 * 4)|= 0xFFUL << (MCUCFG_PENDSVIRQ %  4) * 8
#define mPendSV_Set               *(volatile s_u32_t *)(0xE000E200 + MCUCFG_PENDSVIRQ / 32 * 4) = 0x01UL << (MCUCFG_PENDSVIRQ % 32)
//#define mPendSV_Clear             *(volatile s_u32_t *)(0xE000E280 + MCUCFG_PENDSVIRQ / 32 * 4) = 0x01UL << (MCUCFG_PENDSVIRQ % 32)
#define mPendSV_Clear
#define mPendSV_INIT \
do{ \
	mPendSV_Priority; \
	m_basepri = 1 + ((*(volatile s_u32_t *)0xE000ED00 >> 8) & 7); \
}while(false)
#define mSysINT_Disable           *(volatile s_u32_t *)(0xE000E180 + MCUCFG_PENDSVIRQ / 32 * 4) = (0x01UL << (MCUCFG_SYSTICKIRQ % 32)) \
	                                                                                            | (0x01UL << (MCUCFG_PENDSVIRQ  % 32))
#define mSysINT_Enable            *(volatile s_u32_t *)(0xE000E100 + MCUCFG_PENDSVIRQ / 32 * 4) = (0x01UL << (MCUCFG_SYSTICKIRQ % 32)) \
	                                                                                            | (0x01UL << (MCUCFG_PENDSVIRQ  % 32))
#define mSysIRQ_Disable           __mu_disable_sysirq()
#define mSysIRQ_Enable            __mu_enable_sysirq()
#endif

#define mxDisableIRQ              __mx_disable_irq()
#define mxDisableFIQ              __mx_disable_fiq()
#define mxMaskingPRI(newpri)      __mx_masking_pri(newpri)
#define mxResumeIRQ(oldirq)       __mx_resume_irq(oldirq)
#define mxResumeFIQ(oldfiq)       __mx_resume_fiq(oldfiq)
#define mxResumePRI(oldpri)       __mx_resume_pri(oldpri)

#define mSys_Idle                 __WFI()

#if MCUCFG_HARDWAREFPU == __ENABLED__
#define mFPU_Enable               *(volatile s_u32_t *)0xE000ED88 |= (0x0FUL << 20) /* CP11|CP10 */
#define mSet_ASPEN_LSPEN          *(volatile s_u32_t *)0xE000EF34 |= (0x03UL << 30) /* ASPEN|LSPEN */
#else
#define mFPU_Enable               do{}while(false)
#define mSet_ASPEN_LSPEN          do{}while(false)
#endif

#define mSys_INIT \
do{ \
	mPendSV_P0 = mPendSV_FIFO[0]; \
	mPendSV_P1 = mPendSV_FIFO[1]; \
	__m_set_psp(__m_get_msp() - 2 * MCUCFG_BASICSTACKSIZE); \
	__m_set_control(0x02 | (MCUCFG_HARDWAREFPU == __ENABLED__ ? 0x04 : 0x00)); \
	*(volatile s_u32_t *)0xE000ED14 |= 0x0200; /* 栈8字节对齐 */ \
	mFPU_Enable; \
	mSet_ASPEN_LSPEN; \
	mPendSV_INIT; \
	mSysTick_INIT; \
	mSysTick2_INIT; \
	mSysIRQ_Enable; \
	__asm("cpsie i"); \
}while(false)

#define mSysTick_Counting \
do{ \
	if(tick_temp <= mSysTick_Counter) break; \
	s_tick_counter1 += tick_temp - mSysTick_Counter; \
	s_tick_counter2++; \
}while(false)

#if SYSCFG_DEBUGGING == __ENABLED__
#define mScheduler_INIT \
	m_stacksize_t stacklen; \
	s_sign_schedule = false
#else
#define mScheduler_INIT \
	s_sign_schedule = false
#endif

#define mTaskStack_INIT \
do{ \
	node_news->psp_top = (m_sp_t)node_news->bsp + node_news->stacksize; \
	if(node_news->psp_top % 8){ \
		node_news->psp_top /= 8; \
		node_news->psp_top *= 8; \
		node_news->stacksize = node_news->psp_top - (m_sp_t)node_news->bsp; \
	} \
	node_news->psp = node_news->psp_top; \
	*(s_u32_t *)(node_news->psp - MCUCFG_CALLER_PUSH_FPU - 4) = 0x01000000; /* xPSR */ \
	*(s_u32_t *)(node_news->psp - MCUCFG_CALLER_PUSH_FPU - 8) = (s_u32_t)s_task_starter->entry; /* r15(pc) */ \
	node_news->psp-= MCUCFG_BASICSTACKSIZE; \
}while(false)

#if SYSCFG_DEBUGGING == __ENABLED__
#define mTaskStack_LEN \
	stacklen = s_task_current->psp_top - __m_get_psp() + MCUCFG_CALLEE_PUSH
#else
#define mTaskStack_LEN do{}while(false)
#endif

#define mUsedTime_END \
do{ \
	if(usedtime[0]){ \
		usedtime[0]--; \
		usedtime[1] += mSysTick_InitValue - counter; \
	} \
	else{ \
		if(usedtime[1] >= counter){ \
			usedtime[1] -= counter; \
		} \
		else{ \
			usedtime[1] += mSysTick_InitValue - counter; \
		} \
	} \
	s_task_current->usedtime[0] += usedtime[0]; \
	s_task_current->usedtime[0] += (s_task_current->usedtime[1] + usedtime[1]) / mSysTick_Cycle; \
	s_task_current->usedtime[1]  = (s_task_current->usedtime[1] + usedtime[1]) % mSysTick_Cycle; \
}while(false)

#define mUsedTime_INIT \
do{ \
	usedtime[0] = 0; \
	usedtime[1] = counter; \
}while(false)

#define mTaskStack_PUSH do{}while(false)

#define mTaskStack_POP  mTaskStack_PUSHPOP(sign_push, node_news)

#define mPendSV_Load \
do{ \
	mPendSV_Loader(&u_psv); \
	mPendSV_Set; \
}while(false)

#define mPendSV_Entry \
	if(mPendSV_P0 > mPendSV_FIFO[0]) mPendSV_Handler()

#define miWriteFlagBits \
	if(!u_psv.value){ \
		do{}while(false)



/*
 * STATIC INLINE
 */

__STATIC_INLINE s_u32_t __m_get_msp(void)
{
	register s_u32_t oldmsp;
	__asm("mrs oldmsp, msp");
	return oldmsp;
}

__STATIC_INLINE s_u32_t __m_get_psp(void)
{
	register s_u32_t oldpsp;
	__asm("mrs oldpsp, psp");
	return oldpsp;
}

__STATIC_INLINE void __m_set_psp(s_u32_t newpsp)
{
	__asm("msr psp, newpsp");
}

__STATIC_INLINE void __m_set_control(s_u32_t newctrl)
{
	__asm("msr control, newctrl");
}

__STATIC_INLINE s_u32_t __mx_disable_irq(void)
{
	register s_u32_t oldirq;
	__asm("mrs oldirq, primask");
	__asm("cpsid i");
	__asm("nop");
	return oldirq;
}

__STATIC_INLINE s_u32_t __mx_disable_fiq(void)
{
	register s_u32_t oldfiq;
	__asm("mrs oldfiq, faultmask");
	__asm("cpsid f");
	__asm("nop");
	return oldfiq;
}

__STATIC_INLINE void __mx_resume_irq(s_u32_t oldirq)
{
	__asm("msr primask, oldirq");
}

__STATIC_INLINE void __mx_resume_fiq(s_u32_t oldfiq)
{
	__asm("msr faultmask, oldfiq");
}

__STATIC_INLINE void __mx_resume_pri(s_u32_t oldpri)
{
	__asm("msr basepri, oldpri");
}

#if MCUCFG_SYSINT

__STATIC_INLINE s_u32_t __mx_masking_pri(s_u32_t newpri)
{
	register s_u32_t oldpri;
	newpri <<= m_basepri;
	__asm("mrs oldpri, basepri");
	__asm("msr basepri_max, newpri");
	__asm("dsb");
	__asm("isb");
	return oldpri;
}

__STATIC_INLINE void __mu_enable_sysirq(void)
{
	mSysINT_Enable;
}

__STATIC_INLINE void __mu_disable_sysirq(void)
{
	mSysINT_Disable;
	__asm("dsb");
	__asm("isb");
}

#else

__STATIC_INLINE void __mu_disable_sysirq(void)
{
	__asm("msr basepri, m_basepri");
	__asm("dsb");
	__asm("isb");
}

#endif



#endif
