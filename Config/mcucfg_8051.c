/**************************************************************************//**
 * @item     CosyOS-II Config
 * @file     mcucfg_8051.c
 * @brief    8051 Core Config File
 * @author   迟凯峰
 * @version  V2.2.0
 * @date     2024.04.17
 ******************************************************************************/

#include "..\System\os_var.h"
#ifdef __MCUCFG_8051_H

s_u8_t _SYS_MEM_ m_bsp_add = sizeof(s_taskhand_ts);
s_u8_t _SYS_MEM_ m_reg_add = sizeof(s_tasknode_ts) - 8;
m_sp_t _SYS_MEM_ m_msp;



/*
 * 全局临界区
 */

static bit m_oldirq;
static volatile s_u8_t _SYS_MEM_ m_glocri_counter = 0;

void mx_disable_irq(void)
{
	if(_testbit_(EA)){
		m_oldirq = 1;
	}
	else if(!m_glocri_counter){
		m_oldirq = 0;
	}
	m_glocri_counter++;
}

void mx_resume_irq(void)
{
	m_glocri_counter--;
	if(!m_glocri_counter){
		EA = m_oldirq;
	}
}



/*
 * 中断挂起服务
 */

#if MCUCFG_PENDSVFIFO_DEPTH > 0
#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
s_u8_t mPendSV_FIFO_DepthMAX = 0;
#endif
static s_u16_t mPendSV_FIFO[2][MCUCFG_PENDSVFIFO_DEPTH];
static bit m_sign_fifo = true;
       bit m_sign_fifo_0_0 = true;
static bit m_sign_fifo_1_0 = true;

#define mWriteCode(n) \
static bit m_sign_fifo_0_##n = true; \
static bit m_sign_fifo_1_##n = true

#if MCUCFG_PENDSVFIFO_DEPTH > 1
mWriteCode(1);
#if MCUCFG_PENDSVFIFO_DEPTH > 2
mWriteCode(2);
#if MCUCFG_PENDSVFIFO_DEPTH > 3
mWriteCode(3);
#if MCUCFG_PENDSVFIFO_DEPTH > 4
mWriteCode(4);
#if MCUCFG_PENDSVFIFO_DEPTH > 5
mWriteCode(5);
#if MCUCFG_PENDSVFIFO_DEPTH > 6
mWriteCode(6);
#if MCUCFG_PENDSVFIFO_DEPTH > 7
mWriteCode(7);
#if MCUCFG_PENDSVFIFO_DEPTH > 8
mWriteCode(8);
#if MCUCFG_PENDSVFIFO_DEPTH > 9
mWriteCode(9);
#if MCUCFG_PENDSVFIFO_DEPTH > 10
mWriteCode(10);
#if MCUCFG_PENDSVFIFO_DEPTH > 11
mWriteCode(11);
#if MCUCFG_PENDSVFIFO_DEPTH > 12
mWriteCode(12);
#if MCUCFG_PENDSVFIFO_DEPTH > 13
mWriteCode(13);
#if MCUCFG_PENDSVFIFO_DEPTH > 14
mWriteCode(14);
#if MCUCFG_PENDSVFIFO_DEPTH > 15
mWriteCode(15);
#if MCUCFG_PENDSVFIFO_DEPTH > 16
mWriteCode(16);
#if MCUCFG_PENDSVFIFO_DEPTH > 17
mWriteCode(17);
#if MCUCFG_PENDSVFIFO_DEPTH > 18
mWriteCode(18);
#if MCUCFG_PENDSVFIFO_DEPTH > 19
mWriteCode(19);
#if MCUCFG_PENDSVFIFO_DEPTH > 20
mWriteCode(20);
#if MCUCFG_PENDSVFIFO_DEPTH > 21
mWriteCode(21);
#if MCUCFG_PENDSVFIFO_DEPTH > 22
mWriteCode(22);
#if MCUCFG_PENDSVFIFO_DEPTH > 23
mWriteCode(23);
#if MCUCFG_PENDSVFIFO_DEPTH > 24
mWriteCode(24);
#if MCUCFG_PENDSVFIFO_DEPTH > 25
mWriteCode(25);
#if MCUCFG_PENDSVFIFO_DEPTH > 26
mWriteCode(26);
#if MCUCFG_PENDSVFIFO_DEPTH > 27
mWriteCode(27);
#if MCUCFG_PENDSVFIFO_DEPTH > 28
mWriteCode(28);
#if MCUCFG_PENDSVFIFO_DEPTH > 29
mWriteCode(29);
#if MCUCFG_PENDSVFIFO_DEPTH > 30
mWriteCode(30);
#if MCUCFG_PENDSVFIFO_DEPTH > 31
mWriteCode(31);
#if MCUCFG_PENDSVFIFO_DEPTH > 32
mWriteCode(32);
#if MCUCFG_PENDSVFIFO_DEPTH > 33
mWriteCode(33);
#if MCUCFG_PENDSVFIFO_DEPTH > 34
mWriteCode(34);
#if MCUCFG_PENDSVFIFO_DEPTH > 35
mWriteCode(35);
#if MCUCFG_PENDSVFIFO_DEPTH > 36
mWriteCode(36);
#if MCUCFG_PENDSVFIFO_DEPTH > 37
mWriteCode(37);
#if MCUCFG_PENDSVFIFO_DEPTH > 38
mWriteCode(38);
#if MCUCFG_PENDSVFIFO_DEPTH > 39
mWriteCode(39);
#if MCUCFG_PENDSVFIFO_DEPTH > 40
mWriteCode(40);
#if MCUCFG_PENDSVFIFO_DEPTH > 41
mWriteCode(41);
#if MCUCFG_PENDSVFIFO_DEPTH > 42
mWriteCode(42);
#if MCUCFG_PENDSVFIFO_DEPTH > 43
mWriteCode(43);
#if MCUCFG_PENDSVFIFO_DEPTH > 44
mWriteCode(44);
#if MCUCFG_PENDSVFIFO_DEPTH > 45
mWriteCode(45);
#if MCUCFG_PENDSVFIFO_DEPTH > 46
mWriteCode(46);
#if MCUCFG_PENDSVFIFO_DEPTH > 47
mWriteCode(47);
#if MCUCFG_PENDSVFIFO_DEPTH > 48
mWriteCode(48);
#if MCUCFG_PENDSVFIFO_DEPTH > 49
mWriteCode(49);
#if MCUCFG_PENDSVFIFO_DEPTH > 50
mWriteCode(50);
#if MCUCFG_PENDSVFIFO_DEPTH > 51
mWriteCode(51);
#if MCUCFG_PENDSVFIFO_DEPTH > 52
mWriteCode(52);
#if MCUCFG_PENDSVFIFO_DEPTH > 53
mWriteCode(53);
#if MCUCFG_PENDSVFIFO_DEPTH > 54
mWriteCode(54);
#if MCUCFG_PENDSVFIFO_DEPTH > 55
mWriteCode(55);
#if MCUCFG_PENDSVFIFO_DEPTH > 56

#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif

#undef mWriteCode

static void _fifo_0_(s_u8_t i) MCUCFG_C51USING
{
	void _OBJ_MEM_ *sv = (void _OBJ_MEM_ *)(mPendSV_FIFO[0][i]);
	(*sPendSV_Handler[*(const s_u8_t _OBJ_MEM_ *)sv])(sv);
}

static void _fifo_1_(s_u8_t i) MCUCFG_C51USING
{
	void _OBJ_MEM_ *sv = (void _OBJ_MEM_ *)(mPendSV_FIFO[1][i]);
	(*sPendSV_Handler[*(const s_u8_t _OBJ_MEM_ *)sv])(sv);
}

void mPendSV_Handler(void) MCUCFG_USING
{
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	s_u8_t i;
	#endif
__LABLE:
	m_sign_fifo = false;
	
	m_sign_fifo_0_0 = true;
	      _fifo_0_(0);
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	#define mWriteCode(n) \
	if(m_sign_fifo_0_##n){ i = n; goto __LABLE_0; } \
	   m_sign_fifo_0_##n = true; \
	         _fifo_0_ (n)
	#else
	#define mWriteCode(n) \
	if(m_sign_fifo_0_##n)  goto __LABLE_0; \
	   m_sign_fifo_0_##n = true; \
	         _fifo_0_ (n)
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH > 1
	mWriteCode(1);
	#if MCUCFG_PENDSVFIFO_DEPTH > 2
	mWriteCode(2);
	#if MCUCFG_PENDSVFIFO_DEPTH > 3
	mWriteCode(3);
	#if MCUCFG_PENDSVFIFO_DEPTH > 4
	mWriteCode(4);
	#if MCUCFG_PENDSVFIFO_DEPTH > 5
	mWriteCode(5);
	#if MCUCFG_PENDSVFIFO_DEPTH > 6
	mWriteCode(6);
	#if MCUCFG_PENDSVFIFO_DEPTH > 7
	mWriteCode(7);
	#if MCUCFG_PENDSVFIFO_DEPTH > 8
	mWriteCode(8);
	#if MCUCFG_PENDSVFIFO_DEPTH > 9
	mWriteCode(9);
	#if MCUCFG_PENDSVFIFO_DEPTH > 10
	mWriteCode(10);
	#if MCUCFG_PENDSVFIFO_DEPTH > 11
	mWriteCode(11);
	#if MCUCFG_PENDSVFIFO_DEPTH > 12
	mWriteCode(12);
	#if MCUCFG_PENDSVFIFO_DEPTH > 13
	mWriteCode(13);
	#if MCUCFG_PENDSVFIFO_DEPTH > 14
	mWriteCode(14);
	#if MCUCFG_PENDSVFIFO_DEPTH > 15
	mWriteCode(15);
	#if MCUCFG_PENDSVFIFO_DEPTH > 16
	mWriteCode(16);
	#if MCUCFG_PENDSVFIFO_DEPTH > 17
	mWriteCode(17);
	#if MCUCFG_PENDSVFIFO_DEPTH > 18
	mWriteCode(18);
	#if MCUCFG_PENDSVFIFO_DEPTH > 19
	mWriteCode(19);
	#if MCUCFG_PENDSVFIFO_DEPTH > 20
	mWriteCode(20);
	#if MCUCFG_PENDSVFIFO_DEPTH > 21
	mWriteCode(21);
	#if MCUCFG_PENDSVFIFO_DEPTH > 22
	mWriteCode(22);
	#if MCUCFG_PENDSVFIFO_DEPTH > 23
	mWriteCode(23);
	#if MCUCFG_PENDSVFIFO_DEPTH > 24
	mWriteCode(24);
	#if MCUCFG_PENDSVFIFO_DEPTH > 25
	mWriteCode(25);
	#if MCUCFG_PENDSVFIFO_DEPTH > 26
	mWriteCode(26);
	#if MCUCFG_PENDSVFIFO_DEPTH > 27
	mWriteCode(27);
	#if MCUCFG_PENDSVFIFO_DEPTH > 28
	mWriteCode(28);
	#if MCUCFG_PENDSVFIFO_DEPTH > 29
	mWriteCode(29);
	#if MCUCFG_PENDSVFIFO_DEPTH > 30
	mWriteCode(30);
	#if MCUCFG_PENDSVFIFO_DEPTH > 31
	mWriteCode(31);
	#if MCUCFG_PENDSVFIFO_DEPTH > 32
	mWriteCode(32);
	#if MCUCFG_PENDSVFIFO_DEPTH > 33
	mWriteCode(33);
	#if MCUCFG_PENDSVFIFO_DEPTH > 34
	mWriteCode(34);
	#if MCUCFG_PENDSVFIFO_DEPTH > 35
	mWriteCode(35);
	#if MCUCFG_PENDSVFIFO_DEPTH > 36
	mWriteCode(36);
	#if MCUCFG_PENDSVFIFO_DEPTH > 37
	mWriteCode(37);
	#if MCUCFG_PENDSVFIFO_DEPTH > 38
	mWriteCode(38);
	#if MCUCFG_PENDSVFIFO_DEPTH > 39
	mWriteCode(39);
	#if MCUCFG_PENDSVFIFO_DEPTH > 40
	mWriteCode(40);
	#if MCUCFG_PENDSVFIFO_DEPTH > 41
	mWriteCode(41);
	#if MCUCFG_PENDSVFIFO_DEPTH > 42
	mWriteCode(42);
	#if MCUCFG_PENDSVFIFO_DEPTH > 43
	mWriteCode(43);
	#if MCUCFG_PENDSVFIFO_DEPTH > 44
	mWriteCode(44);
	#if MCUCFG_PENDSVFIFO_DEPTH > 45
	mWriteCode(45);
	#if MCUCFG_PENDSVFIFO_DEPTH > 46
	mWriteCode(46);
	#if MCUCFG_PENDSVFIFO_DEPTH > 47
	mWriteCode(47);
	#if MCUCFG_PENDSVFIFO_DEPTH > 48
	mWriteCode(48);
	#if MCUCFG_PENDSVFIFO_DEPTH > 49
	mWriteCode(49);
	#if MCUCFG_PENDSVFIFO_DEPTH > 50
	mWriteCode(50);
	#if MCUCFG_PENDSVFIFO_DEPTH > 51
	mWriteCode(51);
	#if MCUCFG_PENDSVFIFO_DEPTH > 52
	mWriteCode(52);
	#if MCUCFG_PENDSVFIFO_DEPTH > 53
	mWriteCode(53);
	#if MCUCFG_PENDSVFIFO_DEPTH > 54
	mWriteCode(54);
	#if MCUCFG_PENDSVFIFO_DEPTH > 55
	mWriteCode(55);
	#if MCUCFG_PENDSVFIFO_DEPTH > 56
	
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	
	#undef mWriteCode
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	mPendSV_FIFO_DepthMAX = MCUCFG_PENDSVFIFO_DEPTH;
	#endif
__LABLE_0:
	m_sign_fifo = true;
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	if(i > mPendSV_FIFO_DepthMAX) mPendSV_FIFO_DepthMAX = i;
	#endif
	
	if(m_sign_fifo_1_0) return;
	
	m_sign_fifo_1_0 = true;
	      _fifo_1_(0);
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	#define mWriteCode(n) \
	if(m_sign_fifo_1_##n){ i = n; goto __LABLE_1; } \
	   m_sign_fifo_1_##n = true; \
	         _fifo_1_ (n)
	#else
	#define mWriteCode(n) \
	if(m_sign_fifo_1_##n)  goto __LABLE_1; \
	   m_sign_fifo_1_##n = true; \
	         _fifo_1_ (n)
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH > 1
	mWriteCode(1);
	#if MCUCFG_PENDSVFIFO_DEPTH > 2
	mWriteCode(2);
	#if MCUCFG_PENDSVFIFO_DEPTH > 3
	mWriteCode(3);
	#if MCUCFG_PENDSVFIFO_DEPTH > 4
	mWriteCode(4);
	#if MCUCFG_PENDSVFIFO_DEPTH > 5
	mWriteCode(5);
	#if MCUCFG_PENDSVFIFO_DEPTH > 6
	mWriteCode(6);
	#if MCUCFG_PENDSVFIFO_DEPTH > 7
	mWriteCode(7);
	#if MCUCFG_PENDSVFIFO_DEPTH > 8
	mWriteCode(8);
	#if MCUCFG_PENDSVFIFO_DEPTH > 9
	mWriteCode(9);
	#if MCUCFG_PENDSVFIFO_DEPTH > 10
	mWriteCode(10);
	#if MCUCFG_PENDSVFIFO_DEPTH > 11
	mWriteCode(11);
	#if MCUCFG_PENDSVFIFO_DEPTH > 12
	mWriteCode(12);
	#if MCUCFG_PENDSVFIFO_DEPTH > 13
	mWriteCode(13);
	#if MCUCFG_PENDSVFIFO_DEPTH > 14
	mWriteCode(14);
	#if MCUCFG_PENDSVFIFO_DEPTH > 15
	mWriteCode(15);
	#if MCUCFG_PENDSVFIFO_DEPTH > 16
	mWriteCode(16);
	#if MCUCFG_PENDSVFIFO_DEPTH > 17
	mWriteCode(17);
	#if MCUCFG_PENDSVFIFO_DEPTH > 18
	mWriteCode(18);
	#if MCUCFG_PENDSVFIFO_DEPTH > 19
	mWriteCode(19);
	#if MCUCFG_PENDSVFIFO_DEPTH > 20
	mWriteCode(20);
	#if MCUCFG_PENDSVFIFO_DEPTH > 21
	mWriteCode(21);
	#if MCUCFG_PENDSVFIFO_DEPTH > 22
	mWriteCode(22);
	#if MCUCFG_PENDSVFIFO_DEPTH > 23
	mWriteCode(23);
	#if MCUCFG_PENDSVFIFO_DEPTH > 24
	mWriteCode(24);
	#if MCUCFG_PENDSVFIFO_DEPTH > 25
	mWriteCode(25);
	#if MCUCFG_PENDSVFIFO_DEPTH > 26
	mWriteCode(26);
	#if MCUCFG_PENDSVFIFO_DEPTH > 27
	mWriteCode(27);
	#if MCUCFG_PENDSVFIFO_DEPTH > 28
	mWriteCode(28);
	#if MCUCFG_PENDSVFIFO_DEPTH > 29
	mWriteCode(29);
	#if MCUCFG_PENDSVFIFO_DEPTH > 30
	mWriteCode(30);
	#if MCUCFG_PENDSVFIFO_DEPTH > 31
	mWriteCode(31);
	#if MCUCFG_PENDSVFIFO_DEPTH > 32
	mWriteCode(32);
	#if MCUCFG_PENDSVFIFO_DEPTH > 33
	mWriteCode(33);
	#if MCUCFG_PENDSVFIFO_DEPTH > 34
	mWriteCode(34);
	#if MCUCFG_PENDSVFIFO_DEPTH > 35
	mWriteCode(35);
	#if MCUCFG_PENDSVFIFO_DEPTH > 36
	mWriteCode(36);
	#if MCUCFG_PENDSVFIFO_DEPTH > 37
	mWriteCode(37);
	#if MCUCFG_PENDSVFIFO_DEPTH > 38
	mWriteCode(38);
	#if MCUCFG_PENDSVFIFO_DEPTH > 39
	mWriteCode(39);
	#if MCUCFG_PENDSVFIFO_DEPTH > 40
	mWriteCode(40);
	#if MCUCFG_PENDSVFIFO_DEPTH > 41
	mWriteCode(41);
	#if MCUCFG_PENDSVFIFO_DEPTH > 42
	mWriteCode(42);
	#if MCUCFG_PENDSVFIFO_DEPTH > 43
	mWriteCode(43);
	#if MCUCFG_PENDSVFIFO_DEPTH > 44
	mWriteCode(44);
	#if MCUCFG_PENDSVFIFO_DEPTH > 45
	mWriteCode(45);
	#if MCUCFG_PENDSVFIFO_DEPTH > 46
	mWriteCode(46);
	#if MCUCFG_PENDSVFIFO_DEPTH > 47
	mWriteCode(47);
	#if MCUCFG_PENDSVFIFO_DEPTH > 48
	mWriteCode(48);
	#if MCUCFG_PENDSVFIFO_DEPTH > 49
	mWriteCode(49);
	#if MCUCFG_PENDSVFIFO_DEPTH > 50
	mWriteCode(50);
	#if MCUCFG_PENDSVFIFO_DEPTH > 51
	mWriteCode(51);
	#if MCUCFG_PENDSVFIFO_DEPTH > 52
	mWriteCode(52);
	#if MCUCFG_PENDSVFIFO_DEPTH > 53
	mWriteCode(53);
	#if MCUCFG_PENDSVFIFO_DEPTH > 54
	mWriteCode(54);
	#if MCUCFG_PENDSVFIFO_DEPTH > 55
	mWriteCode(55);
	#if MCUCFG_PENDSVFIFO_DEPTH > 56
	
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	
	#undef mWriteCode
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	mPendSV_FIFO_DepthMAX = MCUCFG_PENDSVFIFO_DEPTH;
	#endif
__LABLE_1:
	do{}while(false);
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	if(i > mPendSV_FIFO_DepthMAX) mPendSV_FIFO_DepthMAX = i;
	#endif
	
	if(!m_sign_fifo_0_0) goto __LABLE;
}

#pragma NOAREGS
void mPendSV_Loader(s_u16_t addr)
{
	if(m_sign_fifo)
	{
		if(!_testbit_(m_sign_fifo_0_0)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 1
		if(!_testbit_(m_sign_fifo_0_1)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 2
		if(!_testbit_(m_sign_fifo_0_2)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 3
		if(!_testbit_(m_sign_fifo_0_3)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 4
		if(!_testbit_(m_sign_fifo_0_4)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 5
		if(!_testbit_(m_sign_fifo_0_5)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 6
		if(!_testbit_(m_sign_fifo_0_6)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 7
		if(!_testbit_(m_sign_fifo_0_7)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 8
		if(!_testbit_(m_sign_fifo_0_8)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 9
		if(!_testbit_(m_sign_fifo_0_9)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
		if(!_testbit_(m_sign_fifo_0_10)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 11
		if(!_testbit_(m_sign_fifo_0_11)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 12
		if(!_testbit_(m_sign_fifo_0_12)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 13
		if(!_testbit_(m_sign_fifo_0_13)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
		goto __LABLE_0_14;
	#endif
		}else{ mPendSV_FIFO[0][13] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][12] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][11] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][10] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][9] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][8] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][7] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][6] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][5] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][4] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][3] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][2] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][1] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][0] = addr; return; }
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
	__LABLE_0_14:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
		if(!_testbit_(m_sign_fifo_0_14)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 15
		if(!_testbit_(m_sign_fifo_0_15)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 16
		if(!_testbit_(m_sign_fifo_0_16)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 17
		if(!_testbit_(m_sign_fifo_0_17)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 18
		if(!_testbit_(m_sign_fifo_0_18)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 19
		if(!_testbit_(m_sign_fifo_0_19)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
		if(!_testbit_(m_sign_fifo_0_20)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 21
		if(!_testbit_(m_sign_fifo_0_21)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 22
		if(!_testbit_(m_sign_fifo_0_22)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 23
		if(!_testbit_(m_sign_fifo_0_23)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 24
		if(!_testbit_(m_sign_fifo_0_24)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 25
		if(!_testbit_(m_sign_fifo_0_25)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 26
		if(!_testbit_(m_sign_fifo_0_26)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 27
		if(!_testbit_(m_sign_fifo_0_27)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
		goto __LABLE_0_28;
	#endif
		}else{ mPendSV_FIFO[0][27] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][26] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][25] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][24] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][23] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][22] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][21] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][20] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][19] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][18] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][17] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][16] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][15] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][14] = addr; return; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
	__LABLE_0_28:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
		if(!_testbit_(m_sign_fifo_0_28)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 29
		if(!_testbit_(m_sign_fifo_0_29)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
		if(!_testbit_(m_sign_fifo_0_30)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 31
		if(!_testbit_(m_sign_fifo_0_31)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 32
		if(!_testbit_(m_sign_fifo_0_32)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 33
		if(!_testbit_(m_sign_fifo_0_33)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 34
		if(!_testbit_(m_sign_fifo_0_34)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 35
		if(!_testbit_(m_sign_fifo_0_35)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 36
		if(!_testbit_(m_sign_fifo_0_36)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 37
		if(!_testbit_(m_sign_fifo_0_37)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 38
		if(!_testbit_(m_sign_fifo_0_38)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 39
		if(!_testbit_(m_sign_fifo_0_39)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
		if(!_testbit_(m_sign_fifo_0_40)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 41
		if(!_testbit_(m_sign_fifo_0_41)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
		goto __LABLE_0_42;
	#endif
		}else{ mPendSV_FIFO[0][41] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][40] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][39] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][38] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][37] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][36] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][35] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][34] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][33] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][32] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][31] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][30] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][29] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][28] = addr; return; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
	__LABLE_0_42:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
		if(!_testbit_(m_sign_fifo_0_42)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 43
		if(!_testbit_(m_sign_fifo_0_43)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 44
		if(!_testbit_(m_sign_fifo_0_44)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 45
		if(!_testbit_(m_sign_fifo_0_45)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 46
		if(!_testbit_(m_sign_fifo_0_46)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 47
		if(!_testbit_(m_sign_fifo_0_47)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 48
		if(!_testbit_(m_sign_fifo_0_48)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 49
		if(!_testbit_(m_sign_fifo_0_49)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
		if(!_testbit_(m_sign_fifo_0_50)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 51
		if(!_testbit_(m_sign_fifo_0_51)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 52
		if(!_testbit_(m_sign_fifo_0_52)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 53
		if(!_testbit_(m_sign_fifo_0_53)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 54
		if(!_testbit_(m_sign_fifo_0_54)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 55
		if(!_testbit_(m_sign_fifo_0_55)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 56
		
	#endif
		}else{ mPendSV_FIFO[0][55] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][54] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][53] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][52] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][51] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][50] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][49] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][48] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][47] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][46] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][45] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][44] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][43] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[0][42] = addr; return; }
	#endif
	}
	else
	{
		if(!_testbit_(m_sign_fifo_1_0)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 1
		if(!_testbit_(m_sign_fifo_1_1)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 2
		if(!_testbit_(m_sign_fifo_1_2)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 3
		if(!_testbit_(m_sign_fifo_1_3)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 4
		if(!_testbit_(m_sign_fifo_1_4)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 5
		if(!_testbit_(m_sign_fifo_1_5)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 6
		if(!_testbit_(m_sign_fifo_1_6)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 7
		if(!_testbit_(m_sign_fifo_1_7)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 8
		if(!_testbit_(m_sign_fifo_1_8)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 9
		if(!_testbit_(m_sign_fifo_1_9)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
		if(!_testbit_(m_sign_fifo_1_10)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 11
		if(!_testbit_(m_sign_fifo_1_11)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 12
		if(!_testbit_(m_sign_fifo_1_12)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 13
		if(!_testbit_(m_sign_fifo_1_13)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
		goto __LABLE_1_14;
	#endif
		}else{ mPendSV_FIFO[1][13] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][12] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][11] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][10] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][9] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][8] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][7] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][6] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][5] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][4] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][3] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][2] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][1] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][0] = addr; return; }
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
	__LABLE_1_14:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
		if(!_testbit_(m_sign_fifo_1_14)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 15
		if(!_testbit_(m_sign_fifo_1_15)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 16
		if(!_testbit_(m_sign_fifo_1_16)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 17
		if(!_testbit_(m_sign_fifo_1_17)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 18
		if(!_testbit_(m_sign_fifo_1_18)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 19
		if(!_testbit_(m_sign_fifo_1_19)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
		if(!_testbit_(m_sign_fifo_1_20)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 21
		if(!_testbit_(m_sign_fifo_1_21)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 22
		if(!_testbit_(m_sign_fifo_1_22)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 23
		if(!_testbit_(m_sign_fifo_1_23)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 24
		if(!_testbit_(m_sign_fifo_1_24)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 25
		if(!_testbit_(m_sign_fifo_1_25)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 26
		if(!_testbit_(m_sign_fifo_1_26)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 27
		if(!_testbit_(m_sign_fifo_1_27)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
		goto __LABLE_1_28;
	#endif
		}else{ mPendSV_FIFO[1][27] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][26] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][25] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][24] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][23] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][22] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][21] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][20] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][19] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][18] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][17] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][16] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][15] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][14] = addr; return; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
	__LABLE_1_28:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
		if(!_testbit_(m_sign_fifo_1_28)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 29
		if(!_testbit_(m_sign_fifo_1_29)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
		if(!_testbit_(m_sign_fifo_1_30)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 31
		if(!_testbit_(m_sign_fifo_1_31)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 32
		if(!_testbit_(m_sign_fifo_1_32)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 33
		if(!_testbit_(m_sign_fifo_1_33)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 34
		if(!_testbit_(m_sign_fifo_1_34)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 35
		if(!_testbit_(m_sign_fifo_1_35)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 36
		if(!_testbit_(m_sign_fifo_1_36)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 37
		if(!_testbit_(m_sign_fifo_1_37)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 38
		if(!_testbit_(m_sign_fifo_1_38)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 39
		if(!_testbit_(m_sign_fifo_1_39)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
		if(!_testbit_(m_sign_fifo_1_40)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 41
		if(!_testbit_(m_sign_fifo_1_41)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
		goto __LABLE_1_42;
	#endif
		}else{ mPendSV_FIFO[1][41] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][40] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][39] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][38] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][37] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][36] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][35] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][34] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][33] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][32] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][31] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][30] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][29] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][28] = addr; return; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
	__LABLE_1_42:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
		if(!_testbit_(m_sign_fifo_1_42)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 43
		if(!_testbit_(m_sign_fifo_1_43)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 44
		if(!_testbit_(m_sign_fifo_1_44)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 45
		if(!_testbit_(m_sign_fifo_1_45)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 46
		if(!_testbit_(m_sign_fifo_1_46)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 47
		if(!_testbit_(m_sign_fifo_1_47)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 48
		if(!_testbit_(m_sign_fifo_1_48)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 49
		if(!_testbit_(m_sign_fifo_1_49)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
		if(!_testbit_(m_sign_fifo_1_50)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 51
		if(!_testbit_(m_sign_fifo_1_51)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 52
		if(!_testbit_(m_sign_fifo_1_52)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 53
		if(!_testbit_(m_sign_fifo_1_53)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 54
		if(!_testbit_(m_sign_fifo_1_54)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 55
		if(!_testbit_(m_sign_fifo_1_55)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 56
		
	#endif
		}else{ mPendSV_FIFO[1][55] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][54] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][53] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][52] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][51] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][50] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][49] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][48] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][47] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][46] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][45] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][44] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][43] = addr; return; }
	#endif
		}else{ mPendSV_FIFO[1][42] = addr; return; }
	#endif
	}
}
#pragma AREGS

#endif



#endif
