/**************************************************************************//**
 * @item     CosyOS-II Config
 * @file     mcucfg_80251.c
 * @brief    80251 Core Config File
 * @author   迟凯峰
 * @version  V2.1.2
 * @date     2024.04.12
 ******************************************************************************/

#include "..\System\os_var.h"
#ifdef __MCUCFG_80251_H

m_sp_t _SYS_MEM_ m_psp[2];
m_sp_t _SYS_MEM_ m_msp;
#if MCUCFG_TASKSTACK_MODE == __MSP__
m_sp_t _SYS_MEM_ m_bsp;
#endif



/*
 * 全局临界区
 */

static bit m_oldirq;
static volatile s_u8_t _SYS_MEM_ m_glocri_counter = 0;

void mx_disable_irq(void)
{
	if(_testbit_(EA)){
		m_oldirq = 1;
	}
	else if(!m_glocri_counter){
		m_oldirq = 0;
	}
	m_glocri_counter++;
}

void mx_resume_irq(void)
{
	m_glocri_counter--;
	if(!m_glocri_counter){
		EA = m_oldirq;
	}
}



/*
 * 中断挂起服务
 */

#if MCUCFG_PENDSVFIFO_DEPTH > 0
#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
s_u8_t mPendSV_FIFO_DepthMAX = 0;
#endif
static void *mPendSV_FIFO[2][MCUCFG_PENDSVFIFO_DEPTH];
static bit ebdata m_sign_fifo = true;
       bit ebdata m_sign_fifo_0_0 = true;
static bit ebdata m_sign_fifo_1_0 = true;

#define mWriteCode(n) \
static bit ebdata m_sign_fifo_0_##n = true; \
static bit ebdata m_sign_fifo_1_##n = true

#if MCUCFG_PENDSVFIFO_DEPTH > 1
mWriteCode(1);
#if MCUCFG_PENDSVFIFO_DEPTH > 2
mWriteCode(2);
#if MCUCFG_PENDSVFIFO_DEPTH > 3
mWriteCode(3);
#if MCUCFG_PENDSVFIFO_DEPTH > 4
mWriteCode(4);
#if MCUCFG_PENDSVFIFO_DEPTH > 5
mWriteCode(5);
#if MCUCFG_PENDSVFIFO_DEPTH > 6
mWriteCode(6);
#if MCUCFG_PENDSVFIFO_DEPTH > 7
mWriteCode(7);
#if MCUCFG_PENDSVFIFO_DEPTH > 8
mWriteCode(8);
#if MCUCFG_PENDSVFIFO_DEPTH > 9
mWriteCode(9);
#if MCUCFG_PENDSVFIFO_DEPTH > 10
mWriteCode(10);
#if MCUCFG_PENDSVFIFO_DEPTH > 11
mWriteCode(11);
#if MCUCFG_PENDSVFIFO_DEPTH > 12
mWriteCode(12);
#if MCUCFG_PENDSVFIFO_DEPTH > 13
mWriteCode(13);
#if MCUCFG_PENDSVFIFO_DEPTH > 14
mWriteCode(14);
#if MCUCFG_PENDSVFIFO_DEPTH > 15
mWriteCode(15);
#if MCUCFG_PENDSVFIFO_DEPTH > 16
mWriteCode(16);
#if MCUCFG_PENDSVFIFO_DEPTH > 17
mWriteCode(17);
#if MCUCFG_PENDSVFIFO_DEPTH > 18
mWriteCode(18);
#if MCUCFG_PENDSVFIFO_DEPTH > 19
mWriteCode(19);
#if MCUCFG_PENDSVFIFO_DEPTH > 20
mWriteCode(20);
#if MCUCFG_PENDSVFIFO_DEPTH > 21
mWriteCode(21);
#if MCUCFG_PENDSVFIFO_DEPTH > 22
mWriteCode(22);
#if MCUCFG_PENDSVFIFO_DEPTH > 23
mWriteCode(23);
#if MCUCFG_PENDSVFIFO_DEPTH > 24
mWriteCode(24);
#if MCUCFG_PENDSVFIFO_DEPTH > 25
mWriteCode(25);
#if MCUCFG_PENDSVFIFO_DEPTH > 26
mWriteCode(26);
#if MCUCFG_PENDSVFIFO_DEPTH > 27
mWriteCode(27);
#if MCUCFG_PENDSVFIFO_DEPTH > 28
mWriteCode(28);
#if MCUCFG_PENDSVFIFO_DEPTH > 29
mWriteCode(29);
#if MCUCFG_PENDSVFIFO_DEPTH > 30
mWriteCode(30);
#if MCUCFG_PENDSVFIFO_DEPTH > 31
mWriteCode(31);
#if MCUCFG_PENDSVFIFO_DEPTH > 32
mWriteCode(32);
#if MCUCFG_PENDSVFIFO_DEPTH > 33
mWriteCode(33);
#if MCUCFG_PENDSVFIFO_DEPTH > 34
mWriteCode(34);
#if MCUCFG_PENDSVFIFO_DEPTH > 35
mWriteCode(35);
#if MCUCFG_PENDSVFIFO_DEPTH > 36
mWriteCode(36);
#if MCUCFG_PENDSVFIFO_DEPTH > 37
mWriteCode(37);
#if MCUCFG_PENDSVFIFO_DEPTH > 38
mWriteCode(38);
#if MCUCFG_PENDSVFIFO_DEPTH > 39
mWriteCode(39);
#if MCUCFG_PENDSVFIFO_DEPTH > 40
mWriteCode(40);
#if MCUCFG_PENDSVFIFO_DEPTH > 41
mWriteCode(41);
#if MCUCFG_PENDSVFIFO_DEPTH > 42
mWriteCode(42);
#if MCUCFG_PENDSVFIFO_DEPTH > 43
mWriteCode(43);
#if MCUCFG_PENDSVFIFO_DEPTH > 44
mWriteCode(44);
#if MCUCFG_PENDSVFIFO_DEPTH > 45
mWriteCode(45);
#if MCUCFG_PENDSVFIFO_DEPTH > 46
mWriteCode(46);
#if MCUCFG_PENDSVFIFO_DEPTH > 47
mWriteCode(47);
#if MCUCFG_PENDSVFIFO_DEPTH > 48
mWriteCode(48);
#if MCUCFG_PENDSVFIFO_DEPTH > 49
mWriteCode(49);
#if MCUCFG_PENDSVFIFO_DEPTH > 50
mWriteCode(50);
#if MCUCFG_PENDSVFIFO_DEPTH > 51
mWriteCode(51);
#if MCUCFG_PENDSVFIFO_DEPTH > 52
mWriteCode(52);
#if MCUCFG_PENDSVFIFO_DEPTH > 53
mWriteCode(53);
#if MCUCFG_PENDSVFIFO_DEPTH > 54
mWriteCode(54);
#if MCUCFG_PENDSVFIFO_DEPTH > 55
mWriteCode(55);
#if MCUCFG_PENDSVFIFO_DEPTH > 56
mWriteCode(56);
#if MCUCFG_PENDSVFIFO_DEPTH > 57
mWriteCode(57);
#if MCUCFG_PENDSVFIFO_DEPTH > 58
mWriteCode(58);
#if MCUCFG_PENDSVFIFO_DEPTH > 59
mWriteCode(59);
#if MCUCFG_PENDSVFIFO_DEPTH > 60
mWriteCode(60);
#if MCUCFG_PENDSVFIFO_DEPTH > 61
mWriteCode(61);
#if MCUCFG_PENDSVFIFO_DEPTH > 62
mWriteCode(62);
#if MCUCFG_PENDSVFIFO_DEPTH > 63
mWriteCode(63);
#if MCUCFG_PENDSVFIFO_DEPTH > 64
mWriteCode(64);
#if MCUCFG_PENDSVFIFO_DEPTH > 65
mWriteCode(65);
#if MCUCFG_PENDSVFIFO_DEPTH > 66
mWriteCode(66);
#if MCUCFG_PENDSVFIFO_DEPTH > 67
mWriteCode(67);
#if MCUCFG_PENDSVFIFO_DEPTH > 68
mWriteCode(68);
#if MCUCFG_PENDSVFIFO_DEPTH > 69
mWriteCode(69);
#if MCUCFG_PENDSVFIFO_DEPTH > 70
mWriteCode(70);
#if MCUCFG_PENDSVFIFO_DEPTH > 71
mWriteCode(71);
#if MCUCFG_PENDSVFIFO_DEPTH > 72
mWriteCode(72);
#if MCUCFG_PENDSVFIFO_DEPTH > 73
mWriteCode(73);
#if MCUCFG_PENDSVFIFO_DEPTH > 74
mWriteCode(74);
#if MCUCFG_PENDSVFIFO_DEPTH > 75
mWriteCode(75);
#if MCUCFG_PENDSVFIFO_DEPTH > 76
mWriteCode(76);
#if MCUCFG_PENDSVFIFO_DEPTH > 77
mWriteCode(77);
#if MCUCFG_PENDSVFIFO_DEPTH > 78
mWriteCode(78);
#if MCUCFG_PENDSVFIFO_DEPTH > 79
mWriteCode(79);
#if MCUCFG_PENDSVFIFO_DEPTH > 80
mWriteCode(80);
#if MCUCFG_PENDSVFIFO_DEPTH > 81
mWriteCode(81);
#if MCUCFG_PENDSVFIFO_DEPTH > 82
mWriteCode(82);
#if MCUCFG_PENDSVFIFO_DEPTH > 83
mWriteCode(83);
#if MCUCFG_PENDSVFIFO_DEPTH > 84
mWriteCode(84);
#if MCUCFG_PENDSVFIFO_DEPTH > 85
mWriteCode(85);
#if MCUCFG_PENDSVFIFO_DEPTH > 86
mWriteCode(86);
#if MCUCFG_PENDSVFIFO_DEPTH > 87
mWriteCode(87);
#if MCUCFG_PENDSVFIFO_DEPTH > 88
mWriteCode(88);
#if MCUCFG_PENDSVFIFO_DEPTH > 89
mWriteCode(89);
#if MCUCFG_PENDSVFIFO_DEPTH > 90
mWriteCode(90);
#if MCUCFG_PENDSVFIFO_DEPTH > 91
mWriteCode(91);
#if MCUCFG_PENDSVFIFO_DEPTH > 92
mWriteCode(92);
#if MCUCFG_PENDSVFIFO_DEPTH > 93
mWriteCode(93);
#if MCUCFG_PENDSVFIFO_DEPTH > 94
mWriteCode(94);
#if MCUCFG_PENDSVFIFO_DEPTH > 95
mWriteCode(95);
#if MCUCFG_PENDSVFIFO_DEPTH > 96
mWriteCode(96);
#if MCUCFG_PENDSVFIFO_DEPTH > 97
mWriteCode(97);
#if MCUCFG_PENDSVFIFO_DEPTH > 98
mWriteCode(98);
#if MCUCFG_PENDSVFIFO_DEPTH > 99
mWriteCode(99);
#if MCUCFG_PENDSVFIFO_DEPTH > 100
mWriteCode(100);
#if MCUCFG_PENDSVFIFO_DEPTH > 101
mWriteCode(101);
#if MCUCFG_PENDSVFIFO_DEPTH > 102
mWriteCode(102);
#if MCUCFG_PENDSVFIFO_DEPTH > 103
mWriteCode(103);
#if MCUCFG_PENDSVFIFO_DEPTH > 104
mWriteCode(104);
#if MCUCFG_PENDSVFIFO_DEPTH > 105
mWriteCode(105);
#if MCUCFG_PENDSVFIFO_DEPTH > 106
mWriteCode(106);
#if MCUCFG_PENDSVFIFO_DEPTH > 107
mWriteCode(107);
#if MCUCFG_PENDSVFIFO_DEPTH > 108
mWriteCode(108);
#if MCUCFG_PENDSVFIFO_DEPTH > 109
mWriteCode(109);
#if MCUCFG_PENDSVFIFO_DEPTH > 110
mWriteCode(110);
#if MCUCFG_PENDSVFIFO_DEPTH > 111
mWriteCode(111);
#if MCUCFG_PENDSVFIFO_DEPTH > 112
mWriteCode(112);
#if MCUCFG_PENDSVFIFO_DEPTH > 113
mWriteCode(113);
#if MCUCFG_PENDSVFIFO_DEPTH > 114
mWriteCode(114);
#if MCUCFG_PENDSVFIFO_DEPTH > 115
mWriteCode(115);
#if MCUCFG_PENDSVFIFO_DEPTH > 116
mWriteCode(116);
#if MCUCFG_PENDSVFIFO_DEPTH > 117
mWriteCode(117);
#if MCUCFG_PENDSVFIFO_DEPTH > 118
mWriteCode(118);
#if MCUCFG_PENDSVFIFO_DEPTH > 119
mWriteCode(119);
#if MCUCFG_PENDSVFIFO_DEPTH > 120

#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif

#undef mWriteCode

static void _fifo_0_(s_u8_t i)
{
	void _OBJ_MEM_ *sv = (void _OBJ_MEM_ *)(mPendSV_FIFO[0][i]);
	(*sPendSV_Handler[*(const s_u8_t _OBJ_MEM_ *)sv])(sv);
}

static void _fifo_1_(s_u8_t i)
{
	void _OBJ_MEM_ *sv = (void _OBJ_MEM_ *)(mPendSV_FIFO[1][i]);
	(*sPendSV_Handler[*(const s_u8_t _OBJ_MEM_ *)sv])(sv);
}

void mPendSV_Handler(void) MCUCFG_USING
{
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	s_u8_t i;
	#endif
__LABLE:
	m_sign_fifo = false;
	
	m_sign_fifo_0_0 = true;
	      _fifo_0_(0);
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	#define mWriteCode(n) \
	if(m_sign_fifo_0_##n){ i = n; goto __LABLE_0; } \
	   m_sign_fifo_0_##n = true; \
	         _fifo_0_ (n)
	#else
	#define mWriteCode(n) \
	if(m_sign_fifo_0_##n)  goto __LABLE_0; \
	   m_sign_fifo_0_##n = true; \
	         _fifo_0_ (n)
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH > 1
	mWriteCode(1);
	#if MCUCFG_PENDSVFIFO_DEPTH > 2
	mWriteCode(2);
	#if MCUCFG_PENDSVFIFO_DEPTH > 3
	mWriteCode(3);
	#if MCUCFG_PENDSVFIFO_DEPTH > 4
	mWriteCode(4);
	#if MCUCFG_PENDSVFIFO_DEPTH > 5
	mWriteCode(5);
	#if MCUCFG_PENDSVFIFO_DEPTH > 6
	mWriteCode(6);
	#if MCUCFG_PENDSVFIFO_DEPTH > 7
	mWriteCode(7);
	#if MCUCFG_PENDSVFIFO_DEPTH > 8
	mWriteCode(8);
	#if MCUCFG_PENDSVFIFO_DEPTH > 9
	mWriteCode(9);
	#if MCUCFG_PENDSVFIFO_DEPTH > 10
	mWriteCode(10);
	#if MCUCFG_PENDSVFIFO_DEPTH > 11
	mWriteCode(11);
	#if MCUCFG_PENDSVFIFO_DEPTH > 12
	mWriteCode(12);
	#if MCUCFG_PENDSVFIFO_DEPTH > 13
	mWriteCode(13);
	#if MCUCFG_PENDSVFIFO_DEPTH > 14
	mWriteCode(14);
	#if MCUCFG_PENDSVFIFO_DEPTH > 15
	mWriteCode(15);
	#if MCUCFG_PENDSVFIFO_DEPTH > 16
	mWriteCode(16);
	#if MCUCFG_PENDSVFIFO_DEPTH > 17
	mWriteCode(17);
	#if MCUCFG_PENDSVFIFO_DEPTH > 18
	mWriteCode(18);
	#if MCUCFG_PENDSVFIFO_DEPTH > 19
	mWriteCode(19);
	#if MCUCFG_PENDSVFIFO_DEPTH > 20
	mWriteCode(20);
	#if MCUCFG_PENDSVFIFO_DEPTH > 21
	mWriteCode(21);
	#if MCUCFG_PENDSVFIFO_DEPTH > 22
	mWriteCode(22);
	#if MCUCFG_PENDSVFIFO_DEPTH > 23
	mWriteCode(23);
	#if MCUCFG_PENDSVFIFO_DEPTH > 24
	mWriteCode(24);
	#if MCUCFG_PENDSVFIFO_DEPTH > 25
	mWriteCode(25);
	#if MCUCFG_PENDSVFIFO_DEPTH > 26
	mWriteCode(26);
	#if MCUCFG_PENDSVFIFO_DEPTH > 27
	mWriteCode(27);
	#if MCUCFG_PENDSVFIFO_DEPTH > 28
	mWriteCode(28);
	#if MCUCFG_PENDSVFIFO_DEPTH > 29
	mWriteCode(29);
	#if MCUCFG_PENDSVFIFO_DEPTH > 30
	mWriteCode(30);
	#if MCUCFG_PENDSVFIFO_DEPTH > 31
	mWriteCode(31);
	#if MCUCFG_PENDSVFIFO_DEPTH > 32
	mWriteCode(32);
	#if MCUCFG_PENDSVFIFO_DEPTH > 33
	mWriteCode(33);
	#if MCUCFG_PENDSVFIFO_DEPTH > 34
	mWriteCode(34);
	#if MCUCFG_PENDSVFIFO_DEPTH > 35
	mWriteCode(35);
	#if MCUCFG_PENDSVFIFO_DEPTH > 36
	mWriteCode(36);
	#if MCUCFG_PENDSVFIFO_DEPTH > 37
	mWriteCode(37);
	#if MCUCFG_PENDSVFIFO_DEPTH > 38
	mWriteCode(38);
	#if MCUCFG_PENDSVFIFO_DEPTH > 39
	mWriteCode(39);
	#if MCUCFG_PENDSVFIFO_DEPTH > 40
	mWriteCode(40);
	#if MCUCFG_PENDSVFIFO_DEPTH > 41
	mWriteCode(41);
	#if MCUCFG_PENDSVFIFO_DEPTH > 42
	mWriteCode(42);
	#if MCUCFG_PENDSVFIFO_DEPTH > 43
	mWriteCode(43);
	#if MCUCFG_PENDSVFIFO_DEPTH > 44
	mWriteCode(44);
	#if MCUCFG_PENDSVFIFO_DEPTH > 45
	mWriteCode(45);
	#if MCUCFG_PENDSVFIFO_DEPTH > 46
	mWriteCode(46);
	#if MCUCFG_PENDSVFIFO_DEPTH > 47
	mWriteCode(47);
	#if MCUCFG_PENDSVFIFO_DEPTH > 48
	mWriteCode(48);
	#if MCUCFG_PENDSVFIFO_DEPTH > 49
	mWriteCode(49);
	#if MCUCFG_PENDSVFIFO_DEPTH > 50
	mWriteCode(50);
	#if MCUCFG_PENDSVFIFO_DEPTH > 51
	mWriteCode(51);
	#if MCUCFG_PENDSVFIFO_DEPTH > 52
	mWriteCode(52);
	#if MCUCFG_PENDSVFIFO_DEPTH > 53
	mWriteCode(53);
	#if MCUCFG_PENDSVFIFO_DEPTH > 54
	mWriteCode(54);
	#if MCUCFG_PENDSVFIFO_DEPTH > 55
	mWriteCode(55);
	#if MCUCFG_PENDSVFIFO_DEPTH > 56
	mWriteCode(56);
	#if MCUCFG_PENDSVFIFO_DEPTH > 57
	mWriteCode(57);
	#if MCUCFG_PENDSVFIFO_DEPTH > 58
	mWriteCode(58);
	#if MCUCFG_PENDSVFIFO_DEPTH > 59
	mWriteCode(59);
	#if MCUCFG_PENDSVFIFO_DEPTH > 60
	mWriteCode(60);
	#if MCUCFG_PENDSVFIFO_DEPTH > 61
	mWriteCode(61);
	#if MCUCFG_PENDSVFIFO_DEPTH > 62
	mWriteCode(62);
	#if MCUCFG_PENDSVFIFO_DEPTH > 63
	mWriteCode(63);
	#if MCUCFG_PENDSVFIFO_DEPTH > 64
	mWriteCode(64);
	#if MCUCFG_PENDSVFIFO_DEPTH > 65
	mWriteCode(65);
	#if MCUCFG_PENDSVFIFO_DEPTH > 66
	mWriteCode(66);
	#if MCUCFG_PENDSVFIFO_DEPTH > 67
	mWriteCode(67);
	#if MCUCFG_PENDSVFIFO_DEPTH > 68
	mWriteCode(68);
	#if MCUCFG_PENDSVFIFO_DEPTH > 69
	mWriteCode(69);
	#if MCUCFG_PENDSVFIFO_DEPTH > 70
	mWriteCode(70);
	#if MCUCFG_PENDSVFIFO_DEPTH > 71
	mWriteCode(71);
	#if MCUCFG_PENDSVFIFO_DEPTH > 72
	mWriteCode(72);
	#if MCUCFG_PENDSVFIFO_DEPTH > 73
	mWriteCode(73);
	#if MCUCFG_PENDSVFIFO_DEPTH > 74
	mWriteCode(74);
	#if MCUCFG_PENDSVFIFO_DEPTH > 75
	mWriteCode(75);
	#if MCUCFG_PENDSVFIFO_DEPTH > 76
	mWriteCode(76);
	#if MCUCFG_PENDSVFIFO_DEPTH > 77
	mWriteCode(77);
	#if MCUCFG_PENDSVFIFO_DEPTH > 78
	mWriteCode(78);
	#if MCUCFG_PENDSVFIFO_DEPTH > 79
	mWriteCode(79);
	#if MCUCFG_PENDSVFIFO_DEPTH > 80
	mWriteCode(80);
	#if MCUCFG_PENDSVFIFO_DEPTH > 81
	mWriteCode(81);
	#if MCUCFG_PENDSVFIFO_DEPTH > 82
	mWriteCode(82);
	#if MCUCFG_PENDSVFIFO_DEPTH > 83
	mWriteCode(83);
	#if MCUCFG_PENDSVFIFO_DEPTH > 84
	mWriteCode(84);
	#if MCUCFG_PENDSVFIFO_DEPTH > 85
	mWriteCode(85);
	#if MCUCFG_PENDSVFIFO_DEPTH > 86
	mWriteCode(86);
	#if MCUCFG_PENDSVFIFO_DEPTH > 87
	mWriteCode(87);
	#if MCUCFG_PENDSVFIFO_DEPTH > 88
	mWriteCode(88);
	#if MCUCFG_PENDSVFIFO_DEPTH > 89
	mWriteCode(89);
	#if MCUCFG_PENDSVFIFO_DEPTH > 90
	mWriteCode(90);
	#if MCUCFG_PENDSVFIFO_DEPTH > 91
	mWriteCode(91);
	#if MCUCFG_PENDSVFIFO_DEPTH > 92
	mWriteCode(92);
	#if MCUCFG_PENDSVFIFO_DEPTH > 93
	mWriteCode(93);
	#if MCUCFG_PENDSVFIFO_DEPTH > 94
	mWriteCode(94);
	#if MCUCFG_PENDSVFIFO_DEPTH > 95
	mWriteCode(95);
	#if MCUCFG_PENDSVFIFO_DEPTH > 96
	mWriteCode(96);
	#if MCUCFG_PENDSVFIFO_DEPTH > 97
	mWriteCode(97);
	#if MCUCFG_PENDSVFIFO_DEPTH > 98
	mWriteCode(98);
	#if MCUCFG_PENDSVFIFO_DEPTH > 99
	mWriteCode(99);
	#if MCUCFG_PENDSVFIFO_DEPTH > 100
	mWriteCode(100);
	#if MCUCFG_PENDSVFIFO_DEPTH > 101
	mWriteCode(101);
	#if MCUCFG_PENDSVFIFO_DEPTH > 102
	mWriteCode(102);
	#if MCUCFG_PENDSVFIFO_DEPTH > 103
	mWriteCode(103);
	#if MCUCFG_PENDSVFIFO_DEPTH > 104
	mWriteCode(104);
	#if MCUCFG_PENDSVFIFO_DEPTH > 105
	mWriteCode(105);
	#if MCUCFG_PENDSVFIFO_DEPTH > 106
	mWriteCode(106);
	#if MCUCFG_PENDSVFIFO_DEPTH > 107
	mWriteCode(107);
	#if MCUCFG_PENDSVFIFO_DEPTH > 108
	mWriteCode(108);
	#if MCUCFG_PENDSVFIFO_DEPTH > 109
	mWriteCode(109);
	#if MCUCFG_PENDSVFIFO_DEPTH > 110
	mWriteCode(110);
	#if MCUCFG_PENDSVFIFO_DEPTH > 111
	mWriteCode(111);
	#if MCUCFG_PENDSVFIFO_DEPTH > 112
	mWriteCode(112);
	#if MCUCFG_PENDSVFIFO_DEPTH > 113
	mWriteCode(113);
	#if MCUCFG_PENDSVFIFO_DEPTH > 114
	mWriteCode(114);
	#if MCUCFG_PENDSVFIFO_DEPTH > 115
	mWriteCode(115);
	#if MCUCFG_PENDSVFIFO_DEPTH > 116
	mWriteCode(116);
	#if MCUCFG_PENDSVFIFO_DEPTH > 117
	mWriteCode(117);
	#if MCUCFG_PENDSVFIFO_DEPTH > 118
	mWriteCode(118);
	#if MCUCFG_PENDSVFIFO_DEPTH > 119
	mWriteCode(119);
	#if MCUCFG_PENDSVFIFO_DEPTH > 120
	
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	
	#undef mWriteCode
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	mPendSV_FIFO_DepthMAX = MCUCFG_PENDSVFIFO_DEPTH;
	#endif
__LABLE_0:
	m_sign_fifo = true;
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	if(i > mPendSV_FIFO_DepthMAX) mPendSV_FIFO_DepthMAX = i;
	#endif
	
	if(m_sign_fifo_1_0) return;
	
	m_sign_fifo_1_0 = true;
	      _fifo_1_(0);
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	#define mWriteCode(n) \
	if(m_sign_fifo_1_##n){ i = n; goto __LABLE_1; } \
	   m_sign_fifo_1_##n = true; \
	         _fifo_1_ (n)
	#else
	#define mWriteCode(n) \
	if(m_sign_fifo_1_##n)  goto __LABLE_1; \
	   m_sign_fifo_1_##n = true; \
	         _fifo_1_ (n)
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH > 1
	mWriteCode(1);
	#if MCUCFG_PENDSVFIFO_DEPTH > 2
	mWriteCode(2);
	#if MCUCFG_PENDSVFIFO_DEPTH > 3
	mWriteCode(3);
	#if MCUCFG_PENDSVFIFO_DEPTH > 4
	mWriteCode(4);
	#if MCUCFG_PENDSVFIFO_DEPTH > 5
	mWriteCode(5);
	#if MCUCFG_PENDSVFIFO_DEPTH > 6
	mWriteCode(6);
	#if MCUCFG_PENDSVFIFO_DEPTH > 7
	mWriteCode(7);
	#if MCUCFG_PENDSVFIFO_DEPTH > 8
	mWriteCode(8);
	#if MCUCFG_PENDSVFIFO_DEPTH > 9
	mWriteCode(9);
	#if MCUCFG_PENDSVFIFO_DEPTH > 10
	mWriteCode(10);
	#if MCUCFG_PENDSVFIFO_DEPTH > 11
	mWriteCode(11);
	#if MCUCFG_PENDSVFIFO_DEPTH > 12
	mWriteCode(12);
	#if MCUCFG_PENDSVFIFO_DEPTH > 13
	mWriteCode(13);
	#if MCUCFG_PENDSVFIFO_DEPTH > 14
	mWriteCode(14);
	#if MCUCFG_PENDSVFIFO_DEPTH > 15
	mWriteCode(15);
	#if MCUCFG_PENDSVFIFO_DEPTH > 16
	mWriteCode(16);
	#if MCUCFG_PENDSVFIFO_DEPTH > 17
	mWriteCode(17);
	#if MCUCFG_PENDSVFIFO_DEPTH > 18
	mWriteCode(18);
	#if MCUCFG_PENDSVFIFO_DEPTH > 19
	mWriteCode(19);
	#if MCUCFG_PENDSVFIFO_DEPTH > 20
	mWriteCode(20);
	#if MCUCFG_PENDSVFIFO_DEPTH > 21
	mWriteCode(21);
	#if MCUCFG_PENDSVFIFO_DEPTH > 22
	mWriteCode(22);
	#if MCUCFG_PENDSVFIFO_DEPTH > 23
	mWriteCode(23);
	#if MCUCFG_PENDSVFIFO_DEPTH > 24
	mWriteCode(24);
	#if MCUCFG_PENDSVFIFO_DEPTH > 25
	mWriteCode(25);
	#if MCUCFG_PENDSVFIFO_DEPTH > 26
	mWriteCode(26);
	#if MCUCFG_PENDSVFIFO_DEPTH > 27
	mWriteCode(27);
	#if MCUCFG_PENDSVFIFO_DEPTH > 28
	mWriteCode(28);
	#if MCUCFG_PENDSVFIFO_DEPTH > 29
	mWriteCode(29);
	#if MCUCFG_PENDSVFIFO_DEPTH > 30
	mWriteCode(30);
	#if MCUCFG_PENDSVFIFO_DEPTH > 31
	mWriteCode(31);
	#if MCUCFG_PENDSVFIFO_DEPTH > 32
	mWriteCode(32);
	#if MCUCFG_PENDSVFIFO_DEPTH > 33
	mWriteCode(33);
	#if MCUCFG_PENDSVFIFO_DEPTH > 34
	mWriteCode(34);
	#if MCUCFG_PENDSVFIFO_DEPTH > 35
	mWriteCode(35);
	#if MCUCFG_PENDSVFIFO_DEPTH > 36
	mWriteCode(36);
	#if MCUCFG_PENDSVFIFO_DEPTH > 37
	mWriteCode(37);
	#if MCUCFG_PENDSVFIFO_DEPTH > 38
	mWriteCode(38);
	#if MCUCFG_PENDSVFIFO_DEPTH > 39
	mWriteCode(39);
	#if MCUCFG_PENDSVFIFO_DEPTH > 40
	mWriteCode(40);
	#if MCUCFG_PENDSVFIFO_DEPTH > 41
	mWriteCode(41);
	#if MCUCFG_PENDSVFIFO_DEPTH > 42
	mWriteCode(42);
	#if MCUCFG_PENDSVFIFO_DEPTH > 43
	mWriteCode(43);
	#if MCUCFG_PENDSVFIFO_DEPTH > 44
	mWriteCode(44);
	#if MCUCFG_PENDSVFIFO_DEPTH > 45
	mWriteCode(45);
	#if MCUCFG_PENDSVFIFO_DEPTH > 46
	mWriteCode(46);
	#if MCUCFG_PENDSVFIFO_DEPTH > 47
	mWriteCode(47);
	#if MCUCFG_PENDSVFIFO_DEPTH > 48
	mWriteCode(48);
	#if MCUCFG_PENDSVFIFO_DEPTH > 49
	mWriteCode(49);
	#if MCUCFG_PENDSVFIFO_DEPTH > 50
	mWriteCode(50);
	#if MCUCFG_PENDSVFIFO_DEPTH > 51
	mWriteCode(51);
	#if MCUCFG_PENDSVFIFO_DEPTH > 52
	mWriteCode(52);
	#if MCUCFG_PENDSVFIFO_DEPTH > 53
	mWriteCode(53);
	#if MCUCFG_PENDSVFIFO_DEPTH > 54
	mWriteCode(54);
	#if MCUCFG_PENDSVFIFO_DEPTH > 55
	mWriteCode(55);
	#if MCUCFG_PENDSVFIFO_DEPTH > 56
	mWriteCode(56);
	#if MCUCFG_PENDSVFIFO_DEPTH > 57
	mWriteCode(57);
	#if MCUCFG_PENDSVFIFO_DEPTH > 58
	mWriteCode(58);
	#if MCUCFG_PENDSVFIFO_DEPTH > 59
	mWriteCode(59);
	#if MCUCFG_PENDSVFIFO_DEPTH > 60
	mWriteCode(60);
	#if MCUCFG_PENDSVFIFO_DEPTH > 61
	mWriteCode(61);
	#if MCUCFG_PENDSVFIFO_DEPTH > 62
	mWriteCode(62);
	#if MCUCFG_PENDSVFIFO_DEPTH > 63
	mWriteCode(63);
	#if MCUCFG_PENDSVFIFO_DEPTH > 64
	mWriteCode(64);
	#if MCUCFG_PENDSVFIFO_DEPTH > 65
	mWriteCode(65);
	#if MCUCFG_PENDSVFIFO_DEPTH > 66
	mWriteCode(66);
	#if MCUCFG_PENDSVFIFO_DEPTH > 67
	mWriteCode(67);
	#if MCUCFG_PENDSVFIFO_DEPTH > 68
	mWriteCode(68);
	#if MCUCFG_PENDSVFIFO_DEPTH > 69
	mWriteCode(69);
	#if MCUCFG_PENDSVFIFO_DEPTH > 70
	mWriteCode(70);
	#if MCUCFG_PENDSVFIFO_DEPTH > 71
	mWriteCode(71);
	#if MCUCFG_PENDSVFIFO_DEPTH > 72
	mWriteCode(72);
	#if MCUCFG_PENDSVFIFO_DEPTH > 73
	mWriteCode(73);
	#if MCUCFG_PENDSVFIFO_DEPTH > 74
	mWriteCode(74);
	#if MCUCFG_PENDSVFIFO_DEPTH > 75
	mWriteCode(75);
	#if MCUCFG_PENDSVFIFO_DEPTH > 76
	mWriteCode(76);
	#if MCUCFG_PENDSVFIFO_DEPTH > 77
	mWriteCode(77);
	#if MCUCFG_PENDSVFIFO_DEPTH > 78
	mWriteCode(78);
	#if MCUCFG_PENDSVFIFO_DEPTH > 79
	mWriteCode(79);
	#if MCUCFG_PENDSVFIFO_DEPTH > 80
	mWriteCode(80);
	#if MCUCFG_PENDSVFIFO_DEPTH > 81
	mWriteCode(81);
	#if MCUCFG_PENDSVFIFO_DEPTH > 82
	mWriteCode(82);
	#if MCUCFG_PENDSVFIFO_DEPTH > 83
	mWriteCode(83);
	#if MCUCFG_PENDSVFIFO_DEPTH > 84
	mWriteCode(84);
	#if MCUCFG_PENDSVFIFO_DEPTH > 85
	mWriteCode(85);
	#if MCUCFG_PENDSVFIFO_DEPTH > 86
	mWriteCode(86);
	#if MCUCFG_PENDSVFIFO_DEPTH > 87
	mWriteCode(87);
	#if MCUCFG_PENDSVFIFO_DEPTH > 88
	mWriteCode(88);
	#if MCUCFG_PENDSVFIFO_DEPTH > 89
	mWriteCode(89);
	#if MCUCFG_PENDSVFIFO_DEPTH > 90
	mWriteCode(90);
	#if MCUCFG_PENDSVFIFO_DEPTH > 91
	mWriteCode(91);
	#if MCUCFG_PENDSVFIFO_DEPTH > 92
	mWriteCode(92);
	#if MCUCFG_PENDSVFIFO_DEPTH > 93
	mWriteCode(93);
	#if MCUCFG_PENDSVFIFO_DEPTH > 94
	mWriteCode(94);
	#if MCUCFG_PENDSVFIFO_DEPTH > 95
	mWriteCode(95);
	#if MCUCFG_PENDSVFIFO_DEPTH > 96
	mWriteCode(96);
	#if MCUCFG_PENDSVFIFO_DEPTH > 97
	mWriteCode(97);
	#if MCUCFG_PENDSVFIFO_DEPTH > 98
	mWriteCode(98);
	#if MCUCFG_PENDSVFIFO_DEPTH > 99
	mWriteCode(99);
	#if MCUCFG_PENDSVFIFO_DEPTH > 100
	mWriteCode(100);
	#if MCUCFG_PENDSVFIFO_DEPTH > 101
	mWriteCode(101);
	#if MCUCFG_PENDSVFIFO_DEPTH > 102
	mWriteCode(102);
	#if MCUCFG_PENDSVFIFO_DEPTH > 103
	mWriteCode(103);
	#if MCUCFG_PENDSVFIFO_DEPTH > 104
	mWriteCode(104);
	#if MCUCFG_PENDSVFIFO_DEPTH > 105
	mWriteCode(105);
	#if MCUCFG_PENDSVFIFO_DEPTH > 106
	mWriteCode(106);
	#if MCUCFG_PENDSVFIFO_DEPTH > 107
	mWriteCode(107);
	#if MCUCFG_PENDSVFIFO_DEPTH > 108
	mWriteCode(108);
	#if MCUCFG_PENDSVFIFO_DEPTH > 109
	mWriteCode(109);
	#if MCUCFG_PENDSVFIFO_DEPTH > 110
	mWriteCode(110);
	#if MCUCFG_PENDSVFIFO_DEPTH > 111
	mWriteCode(111);
	#if MCUCFG_PENDSVFIFO_DEPTH > 112
	mWriteCode(112);
	#if MCUCFG_PENDSVFIFO_DEPTH > 113
	mWriteCode(113);
	#if MCUCFG_PENDSVFIFO_DEPTH > 114
	mWriteCode(114);
	#if MCUCFG_PENDSVFIFO_DEPTH > 115
	mWriteCode(115);
	#if MCUCFG_PENDSVFIFO_DEPTH > 116
	mWriteCode(116);
	#if MCUCFG_PENDSVFIFO_DEPTH > 117
	mWriteCode(117);
	#if MCUCFG_PENDSVFIFO_DEPTH > 118
	mWriteCode(118);
	#if MCUCFG_PENDSVFIFO_DEPTH > 119
	mWriteCode(119);
	#if MCUCFG_PENDSVFIFO_DEPTH > 120
	
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	#endif
	
	#undef mWriteCode
	
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	mPendSV_FIFO_DepthMAX = MCUCFG_PENDSVFIFO_DEPTH;
	#endif
__LABLE_1:
	do{}while(false);
	#if SYSCFG_PENDSVFIFO_MONITOR == __ENABLED__
	if(i > mPendSV_FIFO_DepthMAX) mPendSV_FIFO_DepthMAX = i;
	#endif
	
	if(!m_sign_fifo_0_0) goto __LABLE;
}

void mPendSV_Loader(void *addr)
{
	void **p;
	if(m_sign_fifo)
	{
		if(!_testbit_(m_sign_fifo_0_0)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 1
		if(!_testbit_(m_sign_fifo_0_1)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 2
		if(!_testbit_(m_sign_fifo_0_2)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 3
		if(!_testbit_(m_sign_fifo_0_3)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 4
		if(!_testbit_(m_sign_fifo_0_4)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 5
		if(!_testbit_(m_sign_fifo_0_5)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 6
		if(!_testbit_(m_sign_fifo_0_6)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 7
		if(!_testbit_(m_sign_fifo_0_7)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 8
		if(!_testbit_(m_sign_fifo_0_8)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 9
		if(!_testbit_(m_sign_fifo_0_9)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
		goto __LABLE_0_10;
	#endif
		}else{ p = &mPendSV_FIFO[0][9]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][8]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][7]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][6]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][5]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][4]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][3]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][2]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][1]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][0]; goto __LABLE_0_END; }
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
	__LABLE_0_10:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
		if(!_testbit_(m_sign_fifo_0_10)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 11
		if(!_testbit_(m_sign_fifo_0_11)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 12
		if(!_testbit_(m_sign_fifo_0_12)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 13
		if(!_testbit_(m_sign_fifo_0_13)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
		if(!_testbit_(m_sign_fifo_0_14)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 15
		if(!_testbit_(m_sign_fifo_0_15)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 16
		if(!_testbit_(m_sign_fifo_0_16)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 17
		if(!_testbit_(m_sign_fifo_0_17)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 18
		if(!_testbit_(m_sign_fifo_0_18)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 19
		if(!_testbit_(m_sign_fifo_0_19)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
		goto __LABLE_0_20;
	#endif
		}else{ p = &mPendSV_FIFO[0][19]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][18]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][17]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][16]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][15]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][14]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][13]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][12]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][11]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][10]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
	__LABLE_0_20:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
		if(!_testbit_(m_sign_fifo_0_20)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 21
		if(!_testbit_(m_sign_fifo_0_21)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 22
		if(!_testbit_(m_sign_fifo_0_22)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 23
		if(!_testbit_(m_sign_fifo_0_23)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 24
		if(!_testbit_(m_sign_fifo_0_24)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 25
		if(!_testbit_(m_sign_fifo_0_25)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 26
		if(!_testbit_(m_sign_fifo_0_26)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 27
		if(!_testbit_(m_sign_fifo_0_27)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
		if(!_testbit_(m_sign_fifo_0_28)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 29
		if(!_testbit_(m_sign_fifo_0_29)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
		goto __LABLE_0_30;
	#endif
		}else{ p = &mPendSV_FIFO[0][29]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][28]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][27]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][26]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][25]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][24]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][23]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][22]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][21]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][20]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
	__LABLE_0_30:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
		if(!_testbit_(m_sign_fifo_0_30)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 31
		if(!_testbit_(m_sign_fifo_0_31)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 32
		if(!_testbit_(m_sign_fifo_0_32)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 33
		if(!_testbit_(m_sign_fifo_0_33)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 34
		if(!_testbit_(m_sign_fifo_0_34)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 35
		if(!_testbit_(m_sign_fifo_0_35)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 36
		if(!_testbit_(m_sign_fifo_0_36)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 37
		if(!_testbit_(m_sign_fifo_0_37)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 38
		if(!_testbit_(m_sign_fifo_0_38)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 39
		if(!_testbit_(m_sign_fifo_0_39)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
		goto __LABLE_0_40;
	#endif
		}else{ p = &mPendSV_FIFO[0][39]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][38]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][37]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][36]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][35]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][34]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][33]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][32]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][31]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][30]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
	__LABLE_0_40:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
		if(!_testbit_(m_sign_fifo_0_40)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 41
		if(!_testbit_(m_sign_fifo_0_41)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
		if(!_testbit_(m_sign_fifo_0_42)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 43
		if(!_testbit_(m_sign_fifo_0_43)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 44
		if(!_testbit_(m_sign_fifo_0_44)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 45
		if(!_testbit_(m_sign_fifo_0_45)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 46
		if(!_testbit_(m_sign_fifo_0_46)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 47
		if(!_testbit_(m_sign_fifo_0_47)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 48
		if(!_testbit_(m_sign_fifo_0_48)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 49
		if(!_testbit_(m_sign_fifo_0_49)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
		goto __LABLE_0_50;
	#endif
		}else{ p = &mPendSV_FIFO[0][49]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][48]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][47]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][46]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][45]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][44]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][43]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][42]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][41]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][40]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
	__LABLE_0_50:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
		if(!_testbit_(m_sign_fifo_0_50)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 51
		if(!_testbit_(m_sign_fifo_0_51)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 52
		if(!_testbit_(m_sign_fifo_0_52)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 53
		if(!_testbit_(m_sign_fifo_0_53)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 54
		if(!_testbit_(m_sign_fifo_0_54)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 55
		if(!_testbit_(m_sign_fifo_0_55)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 56
		if(!_testbit_(m_sign_fifo_0_56)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 57
		if(!_testbit_(m_sign_fifo_0_57)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 58
		if(!_testbit_(m_sign_fifo_0_58)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 59
		if(!_testbit_(m_sign_fifo_0_59)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 60
		goto __LABLE_0_60;
	#endif
		}else{ p = &mPendSV_FIFO[0][59]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][58]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][57]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][56]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][55]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][54]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][53]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][52]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][51]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][50]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 60
	__LABLE_0_60:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 60
		if(!_testbit_(m_sign_fifo_0_60)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 61
		if(!_testbit_(m_sign_fifo_0_61)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 62
		if(!_testbit_(m_sign_fifo_0_62)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 63
		if(!_testbit_(m_sign_fifo_0_63)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 64
		if(!_testbit_(m_sign_fifo_0_64)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 65
		if(!_testbit_(m_sign_fifo_0_65)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 66
		if(!_testbit_(m_sign_fifo_0_66)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 67
		if(!_testbit_(m_sign_fifo_0_67)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 68
		if(!_testbit_(m_sign_fifo_0_68)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 69
		if(!_testbit_(m_sign_fifo_0_69)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 70
		goto __LABLE_0_70;
	#endif
		}else{ p = &mPendSV_FIFO[0][69]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][68]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][67]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][66]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][65]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][64]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][63]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][62]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][61]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][60]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 70
	__LABLE_0_70:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 70
		if(!_testbit_(m_sign_fifo_0_70)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 71
		if(!_testbit_(m_sign_fifo_0_71)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 72
		if(!_testbit_(m_sign_fifo_0_72)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 73
		if(!_testbit_(m_sign_fifo_0_73)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 74
		if(!_testbit_(m_sign_fifo_0_74)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 75
		if(!_testbit_(m_sign_fifo_0_75)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 76
		if(!_testbit_(m_sign_fifo_0_76)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 77
		if(!_testbit_(m_sign_fifo_0_77)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 78
		if(!_testbit_(m_sign_fifo_0_78)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 79
		if(!_testbit_(m_sign_fifo_0_79)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 80
		goto __LABLE_0_80;
	#endif
		}else{ p = &mPendSV_FIFO[0][79]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][78]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][77]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][76]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][75]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][74]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][73]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][72]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][71]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][70]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 80
	__LABLE_0_80:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 80
		if(!_testbit_(m_sign_fifo_0_80)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 81
		if(!_testbit_(m_sign_fifo_0_81)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 82
		if(!_testbit_(m_sign_fifo_0_82)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 83
		if(!_testbit_(m_sign_fifo_0_83)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 84
		if(!_testbit_(m_sign_fifo_0_84)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 85
		if(!_testbit_(m_sign_fifo_0_85)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 86
		if(!_testbit_(m_sign_fifo_0_86)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 87
		if(!_testbit_(m_sign_fifo_0_87)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 88
		if(!_testbit_(m_sign_fifo_0_88)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 89
		if(!_testbit_(m_sign_fifo_0_89)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 90
		goto __LABLE_0_90;
	#endif
		}else{ p = &mPendSV_FIFO[0][89]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][88]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][87]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][86]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][85]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][84]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][83]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][82]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][81]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][80]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 90
	__LABLE_0_90:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 90
		if(!_testbit_(m_sign_fifo_0_90)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 91
		if(!_testbit_(m_sign_fifo_0_91)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 92
		if(!_testbit_(m_sign_fifo_0_92)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 93
		if(!_testbit_(m_sign_fifo_0_93)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 94
		if(!_testbit_(m_sign_fifo_0_94)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 95
		if(!_testbit_(m_sign_fifo_0_95)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 96
		if(!_testbit_(m_sign_fifo_0_96)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 97
		if(!_testbit_(m_sign_fifo_0_97)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 98
		if(!_testbit_(m_sign_fifo_0_98)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 99
		if(!_testbit_(m_sign_fifo_0_99)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 100
		goto __LABLE_0_100;
	#endif
		}else{ p = &mPendSV_FIFO[0][99]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][98]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][97]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][96]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][95]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][94]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][93]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][92]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][91]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][90]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 100
	__LABLE_0_100:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 100
		if(!_testbit_(m_sign_fifo_0_100)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 101
		if(!_testbit_(m_sign_fifo_0_101)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 102
		if(!_testbit_(m_sign_fifo_0_102)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 103
		if(!_testbit_(m_sign_fifo_0_103)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 104
		if(!_testbit_(m_sign_fifo_0_104)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 105
		if(!_testbit_(m_sign_fifo_0_105)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 106
		if(!_testbit_(m_sign_fifo_0_106)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 107
		if(!_testbit_(m_sign_fifo_0_107)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 108
		if(!_testbit_(m_sign_fifo_0_108)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 109
		if(!_testbit_(m_sign_fifo_0_109)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 110
		goto __LABLE_0_110;
	#endif
		}else{ p = &mPendSV_FIFO[0][109]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][108]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][107]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][106]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][105]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][104]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][103]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][102]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][101]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][100]; goto __LABLE_0_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 110
	__LABLE_0_110:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 110
		if(!_testbit_(m_sign_fifo_0_110)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 111
		if(!_testbit_(m_sign_fifo_0_111)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 112
		if(!_testbit_(m_sign_fifo_0_112)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 113
		if(!_testbit_(m_sign_fifo_0_113)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 114
		if(!_testbit_(m_sign_fifo_0_114)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 115
		if(!_testbit_(m_sign_fifo_0_115)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 116
		if(!_testbit_(m_sign_fifo_0_116)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 117
		if(!_testbit_(m_sign_fifo_0_117)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 118
		if(!_testbit_(m_sign_fifo_0_118)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 119
		if(!_testbit_(m_sign_fifo_0_119)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 120
	
	#endif
		}else{ p = &mPendSV_FIFO[0][119]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][118]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][117]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][116]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][115]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][114]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][113]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][112]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][111]; goto __LABLE_0_END; }
	#endif
		}else{ p = &mPendSV_FIFO[0][110]; goto __LABLE_0_END; }
	#endif
	
	__LABLE_0_END:
		*p = addr;
	}
	else
	{
		if(!_testbit_(m_sign_fifo_1_0)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 1
		if(!_testbit_(m_sign_fifo_1_1)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 2
		if(!_testbit_(m_sign_fifo_1_2)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 3
		if(!_testbit_(m_sign_fifo_1_3)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 4
		if(!_testbit_(m_sign_fifo_1_4)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 5
		if(!_testbit_(m_sign_fifo_1_5)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 6
		if(!_testbit_(m_sign_fifo_1_6)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 7
		if(!_testbit_(m_sign_fifo_1_7)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 8
		if(!_testbit_(m_sign_fifo_1_8)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 9
		if(!_testbit_(m_sign_fifo_1_9)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
		goto __LABLE_1_10;
	#endif
		}else{ p = &mPendSV_FIFO[1][9]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][8]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][7]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][6]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][5]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][4]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][3]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][2]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][1]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][0]; goto __LABLE_1_END; }
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
	__LABLE_1_10:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 10
		if(!_testbit_(m_sign_fifo_1_10)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 11
		if(!_testbit_(m_sign_fifo_1_11)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 12
		if(!_testbit_(m_sign_fifo_1_12)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 13
		if(!_testbit_(m_sign_fifo_1_13)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 14
		if(!_testbit_(m_sign_fifo_1_14)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 15
		if(!_testbit_(m_sign_fifo_1_15)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 16
		if(!_testbit_(m_sign_fifo_1_16)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 17
		if(!_testbit_(m_sign_fifo_1_17)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 18
		if(!_testbit_(m_sign_fifo_1_18)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 19
		if(!_testbit_(m_sign_fifo_1_19)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
		goto __LABLE_1_20;
	#endif
		}else{ p = &mPendSV_FIFO[1][19]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][18]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][17]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][16]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][15]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][14]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][13]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][12]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][11]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][10]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
	__LABLE_1_20:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 20
		if(!_testbit_(m_sign_fifo_1_20)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 21
		if(!_testbit_(m_sign_fifo_1_21)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 22
		if(!_testbit_(m_sign_fifo_1_22)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 23
		if(!_testbit_(m_sign_fifo_1_23)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 24
		if(!_testbit_(m_sign_fifo_1_24)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 25
		if(!_testbit_(m_sign_fifo_1_25)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 26
		if(!_testbit_(m_sign_fifo_1_26)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 27
		if(!_testbit_(m_sign_fifo_1_27)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 28
		if(!_testbit_(m_sign_fifo_1_28)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 29
		if(!_testbit_(m_sign_fifo_1_29)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
		goto __LABLE_1_30;
	#endif
		}else{ p = &mPendSV_FIFO[1][29]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][28]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][27]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][26]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][25]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][24]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][23]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][22]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][21]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][20]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
	__LABLE_1_30:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 30
		if(!_testbit_(m_sign_fifo_1_30)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 31
		if(!_testbit_(m_sign_fifo_1_31)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 32
		if(!_testbit_(m_sign_fifo_1_32)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 33
		if(!_testbit_(m_sign_fifo_1_33)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 34
		if(!_testbit_(m_sign_fifo_1_34)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 35
		if(!_testbit_(m_sign_fifo_1_35)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 36
		if(!_testbit_(m_sign_fifo_1_36)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 37
		if(!_testbit_(m_sign_fifo_1_37)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 38
		if(!_testbit_(m_sign_fifo_1_38)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 39
		if(!_testbit_(m_sign_fifo_1_39)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
		goto __LABLE_1_40;
	#endif
		}else{ p = &mPendSV_FIFO[1][39]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][38]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][37]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][36]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][35]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][34]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][33]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][32]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][31]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][30]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
	__LABLE_1_40:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 40
		if(!_testbit_(m_sign_fifo_1_40)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 41
		if(!_testbit_(m_sign_fifo_1_41)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 42
		if(!_testbit_(m_sign_fifo_1_42)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 43
		if(!_testbit_(m_sign_fifo_1_43)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 44
		if(!_testbit_(m_sign_fifo_1_44)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 45
		if(!_testbit_(m_sign_fifo_1_45)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 46
		if(!_testbit_(m_sign_fifo_1_46)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 47
		if(!_testbit_(m_sign_fifo_1_47)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 48
		if(!_testbit_(m_sign_fifo_1_48)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 49
		if(!_testbit_(m_sign_fifo_1_49)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
		goto __LABLE_1_50;
	#endif
		}else{ p = &mPendSV_FIFO[1][49]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][48]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][47]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][46]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][45]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][44]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][43]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][42]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][41]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][40]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
	__LABLE_1_50:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 50
		if(!_testbit_(m_sign_fifo_1_50)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 51
		if(!_testbit_(m_sign_fifo_1_51)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 52
		if(!_testbit_(m_sign_fifo_1_52)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 53
		if(!_testbit_(m_sign_fifo_1_53)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 54
		if(!_testbit_(m_sign_fifo_1_54)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 55
		if(!_testbit_(m_sign_fifo_1_55)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 56
		if(!_testbit_(m_sign_fifo_1_56)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 57
		if(!_testbit_(m_sign_fifo_1_57)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 58
		if(!_testbit_(m_sign_fifo_1_58)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 59
		if(!_testbit_(m_sign_fifo_1_59)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 60
		goto __LABLE_1_60;
	#endif
		}else{ p = &mPendSV_FIFO[1][59]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][58]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][57]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][56]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][55]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][54]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][53]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][52]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][51]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][50]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 60
	__LABLE_1_60:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 60
		if(!_testbit_(m_sign_fifo_1_60)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 61
		if(!_testbit_(m_sign_fifo_1_61)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 62
		if(!_testbit_(m_sign_fifo_1_62)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 63
		if(!_testbit_(m_sign_fifo_1_63)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 64
		if(!_testbit_(m_sign_fifo_1_64)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 65
		if(!_testbit_(m_sign_fifo_1_65)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 66
		if(!_testbit_(m_sign_fifo_1_66)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 67
		if(!_testbit_(m_sign_fifo_1_67)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 68
		if(!_testbit_(m_sign_fifo_1_68)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 69
		if(!_testbit_(m_sign_fifo_1_69)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 70
		goto __LABLE_1_70;
	#endif
		}else{ p = &mPendSV_FIFO[1][69]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][68]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][67]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][66]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][65]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][64]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][63]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][62]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][61]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][60]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 70
	__LABLE_1_70:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 70
		if(!_testbit_(m_sign_fifo_1_70)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 71
		if(!_testbit_(m_sign_fifo_1_71)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 72
		if(!_testbit_(m_sign_fifo_1_72)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 73
		if(!_testbit_(m_sign_fifo_1_73)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 74
		if(!_testbit_(m_sign_fifo_1_74)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 75
		if(!_testbit_(m_sign_fifo_1_75)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 76
		if(!_testbit_(m_sign_fifo_1_76)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 77
		if(!_testbit_(m_sign_fifo_1_77)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 78
		if(!_testbit_(m_sign_fifo_1_78)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 79
		if(!_testbit_(m_sign_fifo_1_79)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 80
		goto __LABLE_1_80;
	#endif
		}else{ p = &mPendSV_FIFO[1][79]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][78]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][77]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][76]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][75]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][74]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][73]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][72]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][71]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][70]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 80
	__LABLE_1_80:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 80
		if(!_testbit_(m_sign_fifo_1_80)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 81
		if(!_testbit_(m_sign_fifo_1_81)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 82
		if(!_testbit_(m_sign_fifo_1_82)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 83
		if(!_testbit_(m_sign_fifo_1_83)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 84
		if(!_testbit_(m_sign_fifo_1_84)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 85
		if(!_testbit_(m_sign_fifo_1_85)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 86
		if(!_testbit_(m_sign_fifo_1_86)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 87
		if(!_testbit_(m_sign_fifo_1_87)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 88
		if(!_testbit_(m_sign_fifo_1_88)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 89
		if(!_testbit_(m_sign_fifo_1_89)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 90
		goto __LABLE_1_90;
	#endif
		}else{ p = &mPendSV_FIFO[1][89]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][88]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][87]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][86]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][85]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][84]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][83]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][82]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][81]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][80]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 90
	__LABLE_1_90:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 90
		if(!_testbit_(m_sign_fifo_1_90)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 91
		if(!_testbit_(m_sign_fifo_1_91)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 92
		if(!_testbit_(m_sign_fifo_1_92)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 93
		if(!_testbit_(m_sign_fifo_1_93)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 94
		if(!_testbit_(m_sign_fifo_1_94)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 95
		if(!_testbit_(m_sign_fifo_1_95)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 96
		if(!_testbit_(m_sign_fifo_1_96)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 97
		if(!_testbit_(m_sign_fifo_1_97)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 98
		if(!_testbit_(m_sign_fifo_1_98)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 99
		if(!_testbit_(m_sign_fifo_1_99)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 100
		goto __LABLE_1_100;
	#endif
		}else{ p = &mPendSV_FIFO[1][99]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][98]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][97]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][96]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][95]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][94]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][93]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][92]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][91]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][90]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 100
	__LABLE_1_100:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 100
		if(!_testbit_(m_sign_fifo_1_100)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 101
		if(!_testbit_(m_sign_fifo_1_101)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 102
		if(!_testbit_(m_sign_fifo_1_102)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 103
		if(!_testbit_(m_sign_fifo_1_103)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 104
		if(!_testbit_(m_sign_fifo_1_104)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 105
		if(!_testbit_(m_sign_fifo_1_105)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 106
		if(!_testbit_(m_sign_fifo_1_106)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 107
		if(!_testbit_(m_sign_fifo_1_107)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 108
		if(!_testbit_(m_sign_fifo_1_108)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 109
		if(!_testbit_(m_sign_fifo_1_109)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 110
		goto __LABLE_1_110;
	#endif
		}else{ p = &mPendSV_FIFO[1][109]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][108]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][107]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][106]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][105]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][104]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][103]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][102]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][101]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][100]; goto __LABLE_1_END; }
	#endif
	
	#if MCUCFG_PENDSVFIFO_DEPTH  > 110
	__LABLE_1_110:
	#endif
	#if MCUCFG_PENDSVFIFO_DEPTH  > 110
		if(!_testbit_(m_sign_fifo_1_110)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 111
		if(!_testbit_(m_sign_fifo_1_111)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 112
		if(!_testbit_(m_sign_fifo_1_112)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 113
		if(!_testbit_(m_sign_fifo_1_113)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 114
		if(!_testbit_(m_sign_fifo_1_114)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 115
		if(!_testbit_(m_sign_fifo_1_115)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 116
		if(!_testbit_(m_sign_fifo_1_116)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 117
		if(!_testbit_(m_sign_fifo_1_117)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 118
		if(!_testbit_(m_sign_fifo_1_118)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 119
		if(!_testbit_(m_sign_fifo_1_119)){
	#if MCUCFG_PENDSVFIFO_DEPTH  > 120
	
	#endif
		}else{ p = &mPendSV_FIFO[1][119]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][118]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][117]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][116]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][115]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][114]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][113]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][112]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][111]; goto __LABLE_1_END; }
	#endif
		}else{ p = &mPendSV_FIFO[1][110]; goto __LABLE_1_END; }
	#endif
	
	__LABLE_1_END:
		*p = addr;
	}
}

#endif



#endif
