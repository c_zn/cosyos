/**************************************************************************//**
 * @item     CosyOS-II Kernel
 * @file     os_base.h
 * @brief    基本数据类型定义
 * @author   迟凯峰
 * @version  V2.2.0
 * @date     2024.04.15
 ******************************************************************************/

#ifndef __OS_BASE_H
#define __OS_BASE_H

typedef char  s_s8_t;
typedef short s_s16_t;
typedef long  s_s32_t;
typedef unsigned char  s_u8_t;
typedef unsigned short s_u16_t;
typedef unsigned long  s_u32_t;
typedef s_u8_t s_bool_t;



#endif
