/**************************************************************************//**
 * @item     CosyOS-II Hook
 * @file     pendsv_hook.c
 * @brief    挂起服务钩子
 * @detail   用于安全执行包含读访问的中断挂起服务。
 * @author   迟凯峰
 * @version  V2.0.0
 * @date     2024.03.17
 ******************************************************************************/

#include "..\System\os_link.h"
#if SYSCFG_PENDSVHOOK == __ENABLED__

void pendsv_hook(void) MCUCFG_USING
{
	
}

#endif
